FROM node:lts

WORKDIR /app

COPY . /app/


RUN yarn install
RUN yarn add sharp --ignore-engines
RUN yarn prisma:generate
RUN yarn build


#ENV PATH /app/scripts:$PATH

HEALTHCHECK --interval=1s --timeout=3s CMD curl -f http://localhost:8083/rest/health || false

CMD yarn prisma:deploy && yarn start:prod