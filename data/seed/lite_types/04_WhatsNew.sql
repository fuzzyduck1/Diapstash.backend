INSERT INTO whats_news (id, build_code, release_name, title, description, image) VALUES (1, 118, '1.14.0', 'Notification v2 (1/3)', 'You can now define custom notification content', 'https://diapstash.com/diapstash/whats_news/whats_news_1.png');
INSERT INTO whats_news (id, build_code, release_name, title, description, image) VALUES (2, 118, '1.14.0', 'Notification v2 (2/3)', 'You can now define specific notifications based on the diaper model.', 'https://diapstash.com/diapstash/whats_news/whats_news_2.png');
INSERT INTO whats_news (id, build_code, release_name, title, description, image) VALUES (3, 118, '1.14.0', 'Notification v2 (3/3)', 'You can now configure a night mode for notifications', null);
INSERT INTO whats_news (id, build_code, release_name, title, description, image) VALUES (4, 118, '1.14.0', 'Tracking time', 'Consecutive time spent in diapers is now displayed', 'https://diapstash.com/diapstash/whats_news/whats_news_4.png');
INSERT INTO whats_news (id, build_code, release_name, title, description, image) VALUES (6, 135, '1.15.0', 'Wetness Level', 'You can now indicate the wetness level of your change.', 'https://diapstash.com/diapstash/whats_news/whats_news_6.png');
INSERT INTO whats_news (id, build_code, release_name, title, description, image) VALUES (7, 135, '1.15.0', 'Custom statistics period', 'You can browse your statistics over a customized period of time. ', 'https://diapstash.com/diapstash/whats_news/whats_news_7.png');
INSERT INTO whats_news (id, build_code, release_name, title, description, image) VALUES (8, 135, '1.15.0', 'Randomly select your diaper', 'Don''t know which diaper to wear? Choose it randomly. ', 'https://diapstash.com/diapstash/whats_news/whats_news_8.png');
INSERT INTO whats_news (id, build_code, release_name, title, description, image) VALUES (9, 155, '1.16.0', 'Diaper Price (1/3)', e'You can now add the price of the diapers you add to your stock.
From the catalog, you can choose whether to define a price per diaper or a total price for the number of diapers added.
This information is optional', 'https://diapstash.com/diapstash/whats_news/wn_1_16_diaper_price_1.png');
INSERT INTO whats_news (id, build_code, release_name, title, description, image) VALUES (10, 155, '1.16.0', 'Diaper Price (2/3)', 'You can view and edit the price of your diapers on the stock editing dialog. (click on the circled i icon in your stock). ', 'https://diapstash.com/diapstash/whats_news/wn_1_16_diaper_price_2.png');
INSERT INTO whats_news (id, build_code, release_name, title, description, image) VALUES (11, 155, '1.16.0', 'Diaper Price (3/3)', e'Exchange prices can be consulted on the change edit dialog.
An average is also available in your stats', 'https://diapstash.com/diapstash/whats_news/wn_1_16_diaper_price_3.png');

SELECT setval('whats_news_id_seq', (SELECT MAX(id) FROM "whats_news"));
