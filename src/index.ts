import {$log} from "@tsed/common";
import { PlatformExpress } from "@tsed/platform-express";
import {Server} from "./Server";

import * as Sentry from "@sentry/node";
import { SENTRY_DSN } from "./config/envs";

async function bootstrap() {
  try {

    if(SENTRY_DSN != null){
      Sentry.init({
        dsn: SENTRY_DSN,
        integrations: [
          new Sentry.Integrations.Http({ tracing: true })
        ],
        tracesSampleRate: 1.0,
        debug: false,
        beforeSendTransaction(event) {
          if (event.transaction === "GET /rest/metrics/prisma") {
            return null;
          }else if (event.transaction === "GET /metrics") {
            return null;
          }
          return event;
        },
      });
    }


    const platform = await PlatformExpress.bootstrap(Server);

    await platform.listen();
    process.on("SIGINT", () => {
      platform.stop();
    });
  } catch (error) {
    $log.error({event: "SERVER_BOOTSTRAP_ERROR", message: error.message, stack: error.stack});
  }
}

bootstrap();
