import {useDecorators} from "@tsed/core";
import {Authenticate, AuthorizeOptions} from "@tsed/passport";
import {In, OneOf, Returns, Security} from "@tsed/schema";
import {Unauthorized} from "@tsed/exceptions";
import { AppAuthOptional } from "./appAuth";
import { JwtAuthOptional } from "./jwtAuth";

/**
 * Set BasicAuth access on decorated route
 * @param JwtAuth
 */
export function AppJwtAuth(options: AuthorizeOptions = {}) {
  return useDecorators(
    Authenticate(['jwt', 'app'], options),
    Returns(401, Unauthorized).Description("Unauthorized app or jwt"),
  );
}

export function AppJwtAuthOptional(options: AuthorizeOptions = {}) {
  return useDecorators(
    Authenticate(['jwt', 'app'], {...options, optional: true} as any),
  );
}