import {UseBefore, Middleware, Req, Context} from "@tsed/common";
import {StoreSet, useDecorators} from "@tsed/core";
import {Forbidden} from "@tsed/exceptions";
import {Returns} from "@tsed/schema";

@Middleware()
export class AuthRolesMiddleware {
  use(@Req("user") user: any, @Context() ctx: Context) {
    if (user) {
      const roles = ctx.endpoint.get(AuthRolesMiddleware) as string[];
      if (!roles.some((r: string) => user.roles.includes(r))) {
        throw new Forbidden("Insufficient role");
      }
    }
  }
}

export function AuthRoles(...roles: string[]) {
  return useDecorators(
    UseBefore(AuthRolesMiddleware),
    StoreSet(AuthRolesMiddleware, roles),
    Returns(403, Forbidden).Description("Forbidden")
  );
}
