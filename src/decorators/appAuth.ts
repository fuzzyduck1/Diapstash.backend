import {useDecorators} from "@tsed/core";
import {Authenticate, AuthorizeOptions} from "@tsed/passport";
import {In, Returns, Security} from "@tsed/schema";
import {Unauthorized} from "@tsed/exceptions";

/**
 * Set BasicAuth access on decorated route
 * @param JwtAuth
 */
export function AppAuth(options: AuthorizeOptions = {}) {
  return useDecorators(
    Authenticate("app", options),
    Security("app"),
    Returns(401, Unauthorized).Description("Unauthorized"),
    In("header").Name("X-USER-ID").Description("User Id").Type(String).Required(true),
    In("header").Name("X-SECURE-TOKEN").Description("Secure Token").Type(String).Required(true)
  );
}

export function AppAuthOptional(options: AuthorizeOptions = {}) {
  return useDecorators(
    Authenticate("app", {...options, optional: true} as any),
    Security("app"),
    //Returns(401, Unauthorized).Description("Unauthorized"),
    In("header").Name("X-USER-ID").Description("User Id").Type(String).Required(false),
    In("header").Name("X-SECURE-TOKEN").Description("Secure Token").Type(String).Required(false)
  );
}
