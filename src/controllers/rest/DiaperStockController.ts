import { DiaperStock, Prisma } from "@prisma/client";
import { Controller, Inject } from "@tsed/di";
import { UseAuth } from "@tsed/platform-middlewares";
import { BodyParams, HeaderParams } from "@tsed/platform-params";
import { ContentType, Delete, Get, Post } from "@tsed/schema";
import { AppUserAuthMiddleware } from "../../middlewares/AppUserAuthMiddleware";
import { NotFound } from "@tsed/exceptions";
import { PrismaService } from "@tsed/prisma";

@Controller("/diaper-stock")
export class DiaperStockController {

  @Inject()
  prisma: PrismaService;

  @Get("/")
  @UseAuth(AppUserAuthMiddleware)
  @ContentType("application/json")
  async get(@HeaderParams("x-user-id") userId: string) {
    return await this.prisma.diaperStock.findMany({where: {userId: userId}, include: {diaperType: true}});
  }

  @Post("/")
  @UseAuth(AppUserAuthMiddleware)
  @ContentType("application/json")
  async post(@BodyParams() stock: DiaperStock, @HeaderParams("x-user-id") userId: string){
    stock.userId = userId;

    let counts = stock.counts as Prisma.JsonObject;
    let prices = stock.prices as Prisma.JsonObject;

    return await this.prisma.diaperStock.upsert({
      where: {
        diaperTypeId_userId: {
          userId: stock.userId,
          diaperTypeId: stock.diaperTypeId
        }
      },
      update: {
        counts: counts,
        prices: prices
      },
      create: {
        counts: counts,
        prices: prices,
        userId: stock.userId,
        diaperTypeId: stock.diaperTypeId
      }
    });

  }

  @Delete("/")
  @UseAuth(AppUserAuthMiddleware)
  @ContentType("application/json")
  async delete(@BodyParams() stock: DiaperStock, @HeaderParams("x-user-id") userId: string){
    if(stock.diaperTypeId >= 0){
      return await this.prisma.diaperStock.delete({
        where: {
          diaperTypeId_userId: {
            userId: userId,
            diaperTypeId: stock.diaperTypeId
          }
        }
      });
    }else{
      throw new NotFound("DiaperType are custom but not sync");
    }
  }
}
