import { DiaperBrand } from "@prisma/client";
import {Controller, Inject} from "@tsed/di";
import { BodyParams, PathParams } from "@tsed/platform-params";
import {ContentType, Get, Post} from "@tsed/schema";
import { AuthRoles } from "../../decorators/authRoles";
import { JwtAuth } from "../../decorators/jwtAuth";
import { PrismaService } from "@tsed/prisma";


@Controller("/diaper-brand")
export class DiaperBrandController {
  @Inject()
  prisma: PrismaService;

  @Get("/")
  @ContentType("application/json")
  async get() {
    return await this.prisma.diaperBrand.findMany({});
  }

  @Get("/:code")
  @ContentType("application/json")
  async getBrand(@PathParams("code") code: string) {
    return await this.prisma.diaperBrand.findFirst({
      where: {
        code: code
      }
    });
  }

  @Post("/")
  @ContentType("application/json")
  @JwtAuth()
  @AuthRoles('ADMIN')
  async post(@BodyParams() brand: DiaperBrand) {
    return await this.prisma.diaperBrand.upsert({
      where: {code: brand.code},
      create: brand,
      update: {
        name: brand.name,
        image: brand.image
      }
    })
  }

  // @Post("/import")
  // @JwtAuth()
  // @AuthRoles('ADMIN')
  // async import(@BodyParams() brands: DiaperBrand[]){


  //   for(let brand of brands){
  //     // console.log(brand);
  //     await this.prisma.diaperBrand.upsert({
  //       where: {code: brand.code},
  //       create: brand,
  //       update: brand
  //     });
  //   }
  // }

}
