import { Controller, Inject } from "@tsed/di";
import { BodyParams } from "@tsed/platform-params";
import { Post } from "@tsed/schema";
import axios from "axios";
import sharp from "sharp";
import * as minio from "minio";
import { IMAGE_URL, S3_ACCESS_KEY, S3_BUCKET, S3_HOST, S3_SECRET_KEY, NO_CACHE_DOMAIN, REMOVE_BG_API} from "../../config/envs";
import { removeBackgroundFromImageUrl } from "remove.bg";
import { JwtAuth } from "../../decorators/jwtAuth";
import { AuthRoles } from "../../decorators/authRoles";
import { PrismaService } from "@tsed/prisma";


const MIME_PNG = 'image/png';


@Controller("/upload")
export class UploadController {

  private minioClient: minio.Client | null = null;

  @Inject()
  prisma: PrismaService;

  getMinioClient() {
    if (this.minioClient == null) {
      this.minioClient = new minio.Client({
        endPoint: S3_HOST!,
        port: 443,
        useSSL: true,
        accessKey: S3_ACCESS_KEY!,
        secretKey: S3_SECRET_KEY!
      });
    }
    return this.minioClient;
  }

  @Post("/image")
  async uploadImage(@BodyParams('image') image: string, @BodyParams('path') path: string) {
    let res = await axios.get(image, {
      responseType: 'arraybuffer',
      headers: {
        'Accept-Encoding': 'zlib'
      },
      decompress: true
    });
    let buff = Buffer.from(res.data);
    let img = await sharp(buff);

    var bytes = await img.png().toBuffer();

    await this.getMinioClient().putObject(S3_BUCKET!, path, bytes, bytes.length, { 'Content-Type': MIME_PNG });
    let outUrl = `${IMAGE_URL}${path.replace('/', '')}`;
    return {
      url: outUrl
    };
  }

  @Post("/removebg")
  @JwtAuth()
  @AuthRoles('ADMIN', 'MASS_EDITOR')
  async uploadremovebg(@BodyParams('image') image: string, @BodyParams('imagePath') imagePath?: string) {
    console.log('Remove bg', `'${image}'`, imagePath);
    let url = new URL(image);

    try {
      let res = await removeBackgroundFromImageUrl({
        url: image.replace('diapstash.com', NO_CACHE_DOMAIN),
        apiKey: REMOVE_BG_API,
        size: "auto",
        type: "product",
        format: "png",
        position: "center",
        add_shadow: false
      });


      let buff = Buffer.from(res.base64img, 'base64');

      let minioPath = imagePath ?? `/removebg${url.pathname}`;

      await this.getMinioClient().putObject(S3_BUCKET!, minioPath, buff, buff.length, { 'Content-Type': MIME_PNG });
      let outUrl = `${IMAGE_URL}${minioPath.replace('/', '')}`;
      return {
        url: outUrl
      };
    } catch (e) {
      console.log('Error on removebg', e);
      throw e;
    }
  }


  @Post("/optimize")
  @JwtAuth()
  @AuthRoles('ADMIN', 'MASS_EDITOR')
  async optimize(@BodyParams('image') image: string, @BodyParams('imagePath') imagePath?: string) {
    console.log('optimize', `'${image}'`, imagePath);
    let url = new URL(image);

    try {
      let res = await axios.get(image.replace('diapstash.com', NO_CACHE_DOMAIN), {
        responseType: 'arraybuffer',
        headers: {
          'Accept-Encoding': 'zlib'
        },
        decompress: true
      });
      let buff = Buffer.from(res.data);

      let img = await sharp(buff);

      let height = (await img.metadata()).height ?? 0;
      if(height > 1000){
        img = img.resize({height: 1000, fit: "contain"});
      }

      var bytes = await img.png().toBuffer();

      let minioPath = imagePath ?? `/removebg${url.pathname}`;

      await this.getMinioClient().putObject(S3_BUCKET!, minioPath, bytes, bytes.length, { 'Content-Type': MIME_PNG });
      let outUrl = `${IMAGE_URL}${minioPath.replace('/', '')}`;
      return {
        url: outUrl
      };
    } catch (e) {
      console.log('Error on removebg', e);
      throw e;
    }
  }
}
