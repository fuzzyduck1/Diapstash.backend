import { DiaperType } from "@prisma/client";
import { Req } from "@tsed/common";
import { Controller, Inject } from "@tsed/di";
import { NotFound } from "@tsed/exceptions";
import { UseAuth } from "@tsed/platform-middlewares";
import { BodyParams, HeaderParams, PathParams } from "@tsed/platform-params";
import { ContentType, Delete, Get, Post, Put, Summary } from "@tsed/schema";
import { AppUserAuthMiddleware, getUserFromReq } from "../../middlewares/AppUserAuthMiddleware";
import { AuthRoles } from "../../decorators/authRoles";
import { JwtAuth } from "../../decorators/jwtAuth";
import { PrismaService } from "@tsed/prisma";

interface IImportType {
  brand_code: string;
  name: string;
  size: string;
  image: string;
}

@Controller("/diaper-type")
export class DiaperTypeController {

  @Inject()
  prisma: PrismaService;

  @Get("/")
  @Summary("Load all diaper types")
  @ContentType("application/json")
  async get(@Req() req: Req, @HeaderParams("x-user-id") userId: string) {
    let orConditions: any[] = [{ official: true }];
    if (userId != null) {
      let user = await getUserFromReq(this.prisma, req);
      if(user != null){
        orConditions.push({ userId: userId });
      }
    }
    return await this.prisma.diaperType.findMany({ where: { OR: orConditions }, include: { brand: true } });
  }

  @Get("/:id")
  @Summary("Find by Id")
  @ContentType("application/json")
  @JwtAuth()
  @AuthRoles('ADMIN', 'MASS_EDITOR')
  async getId(@PathParams("id") id: number) {
    return await this.prisma.diaperType.findFirst({ where: { id: id }, include: { brand: true } });
  }


  @Post("/")
  @ContentType("application/json")
  @JwtAuth()
  @AuthRoles('ADMIN', 'MASS_EDITOR')
  async post(@BodyParams() type: DiaperType) {
    delete (type as any)['brand'];
    if (type.id == null) {
      return await this.prisma.diaperType.create({ data: type });
    }
    return await this.prisma.diaperType.upsert({
      where: { id: type.id },
      create: type,
      update: {
        name: type.name,
        availableSizes: type.availableSizes,
        image: type.image,
        brand_code: type.brand_code,
        type: type.type,
        style: type.style,
        target: type.target,
        discontinued: type.discontinued
      }
    })
  }

  @Put("/user")
  @ContentType("application/json")
  @UseAuth(AppUserAuthMiddleware)
  async putUser(@BodyParams() type: DiaperType, @HeaderParams("x-user-id") userId: string) {
    delete (type as any)['brand'];
    delete (type as any)['id'];

    type.userId = userId;

    return await this.prisma.diaperType.create({
      data: type,
    })
  }

  @Post("/user")
  @ContentType("application/json")
  @UseAuth(AppUserAuthMiddleware)
  async posttUser(@BodyParams() type: DiaperType, @HeaderParams("x-user-id") userId: string) {
    delete (type as any)['brand'];

    type.userId = userId;

    let bddType = await this.prisma.diaperType.findFirst({
      where: {
        id: type.id,
        userId: userId
      }
    })

    if (bddType != null) {

      return await this.prisma.diaperType.update({
        where: { id: type.id },
        data: {
          name: type.name,
          availableSizes: type.availableSizes,
          image: type.image,
          brand_code: type.brand_code,
          type: type.type,
          style: type.style,
          target: type.target,
          discontinued: type.discontinued
        }
      })
    } else {
      throw new NotFound("");
    }
  }


  @Get("/user/deleted")
  @Summary("Find by Id")
  @ContentType("application/json")
  @UseAuth(AppUserAuthMiddleware)
  async getDeleted(@HeaderParams("x-user-id") userId: string) {
    return await this.prisma.deletedDiaperType.findMany({ where: { userId: userId }});
  }

  @Delete("/user")
  @ContentType("application/json")
  @UseAuth(AppUserAuthMiddleware)
  async deleteUser(@BodyParams() type: DiaperType, @HeaderParams("x-user-id") userId: string) {
    delete (type as any)['brand'];

    type.userId = userId;

    let bddType = await this.prisma.diaperType.findFirst({
      where: {
        id: type.id,
        userId: userId,
        official: false
      }
    });

    if (bddType != null) {

      await this.prisma.deletedDiaperType.create({
        data: {
          userId: userId,
          diaperTypeId: type.id
        },
      });

      return await this.prisma.diaperType.delete({
        where: { id: type.id },

      })
    } else {
      throw new NotFound("");
    }
  }

}
