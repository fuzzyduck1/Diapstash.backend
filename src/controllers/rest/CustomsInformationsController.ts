import {Controller, Inject} from "@tsed/di";
import {Delete, Get, Post, Property} from "@tsed/schema";
import { AppAuth } from "../../decorators/appAuth";
import { PrismaClient, User } from "@prisma/client";
import { BodyParams, Req } from "@tsed/common";


interface CustomInformationReq {
  id: string;
  name: string;
  type: string;
  customData: any;

  updatedAt: string;
}

interface CustomInformationDataReq {
  customInformationId: string;
  typeId: number;
  raw: string;

  updatedAt: string;
}


@Controller("/customs-informations")
export class CustomsInformationsController {

  @Inject()
  prisma: PrismaClient;

  @Get("/")
  @AppAuth()
  async get(@Req() req: Req) {
    let user = req.user! as User;
    return await this.prisma.customInformation.findMany({
      where: {userId: user.id }
    });
  }

  @Post("/")
  @AppAuth()
  async add(@Req() req: Req, @BodyParams() info: CustomInformationReq) {
    const user = req.user! as User;
    return await this.prisma.customInformation.upsert({
      create: {
        userId: user.id,
        id: info.id,
        name: info.name,
        type: info.type,
        customData: info.customData,
        updatedAt: new Date(info.updatedAt)
      },
      update: {
        name: info.name,
        type: info.type,
        customData: info.customData,
        updatedAt: new Date(info.updatedAt)
      },
      where: {
        id: info.id,
        userId: user.id
      }
    });
  }

  @Delete("/")
  @AppAuth()
  async delete(@Req() req: Req, @BodyParams() info: CustomInformationReq) {
    return await this.prisma.customInformation.delete({
      where: {
        id: info.id,
        userId: (req.user! as User).id
      }
    });
  }

  @Post("/many")
  @AppAuth()
  async addMany(@Req() req: Req, @BodyParams() infos: CustomInformationReq[]) {
    console.log(infos);
    const user = (req.user as User);
    for(var info of infos){
      return await this.prisma.customInformation.upsert({
        create: {
          userId: user.id,
          ...info,
          updatedAt: new Date(info.updatedAt)
        },
        update: {
          name: info.name,
          type: info.type,
          customData: info.customData,
          updatedAt: new Date(info.updatedAt),
        },
        where: {
          id: info.id,
          userId: user.id
        }
      });
    }
  }


  @Get("/data")
  @AppAuth()
  async getData(@Req() req: Req) {
    let user = req.user! as User;
    return await this.prisma.customInformationData.findMany({
      where: { customInfrmation: {userId: user.id} }
    });
  }

  @Post("/data")
  @AppAuth()
  async addData(@Req() req: Req, @BodyParams() data: CustomInformationDataReq) {
    const user = req.user! as User;
    return await this.prisma.customInformationData.upsert({
      create: {
        ...data,
        updatedAt: new Date(data.updatedAt)
      },
      update: {
        raw: data.raw,
        updatedAt: new Date(data.updatedAt)
      },
      where: {
        typeId_customInformationId: {
          customInformationId: data.customInformationId,
          typeId: data.typeId
        }
      }
    });
  }

  @Delete("/data")
  @AppAuth()
  async deleteData(@Req() req: Req, @BodyParams() data: CustomInformationDataReq) {
    return await this.prisma.customInformationData.delete({
      where: {
        typeId_customInformationId: {
            customInformationId: data.customInformationId,
            typeId: data.typeId
          }
      }
    });
  }

  @Post("/data/many")
  @AppAuth()
  async addManyData(@Req() req: Req, @BodyParams() datas: CustomInformationDataReq[]) {
    const user = (req.user as User);
    for(var data of datas){
      return await this.prisma.customInformationData.upsert({
        create: {
          ...data,
          updatedAt: new Date(data.updatedAt)
        },
        update: {
          raw: data.raw,
          updatedAt: new Date(data.updatedAt)
        },
        where: {
          typeId_customInformationId: {
            customInformationId: data.customInformationId,
            typeId: data.typeId
          }
        }
      });
    }
  }

}
