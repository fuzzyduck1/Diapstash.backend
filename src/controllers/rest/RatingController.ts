import { Rating } from "@prisma/client";
import { UseAuth } from "@tsed/common";
import { PlatformCache, UseCache } from "@tsed/platform-cache";
import {Controller, Inject} from "@tsed/di";
import { BodyParams, HeaderParams, PathParams } from "@tsed/platform-params";
import {ContentType, Get, Post} from "@tsed/schema";
import { AppUserAuthMiddleware } from "../../middlewares/AppUserAuthMiddleware";
import * as Sentry from "@sentry/node";
import { Forbidden } from "@tsed/exceptions";
import { PrismaService } from "@tsed/prisma";

interface ReqRatingSet {
  id?: string | null;
  criteria: string;
  type_id: number;
  note: number;
}

interface ReqLinkCloudSync {
  ids: string[]
}

interface ReqUnlinkCloudSync {
  recreateRating: boolean
}

@Controller("/rating")
export class RatingController {

  @Inject()
  prisma: PrismaService;

  @Inject()
  cache: PlatformCache;

  @Get("/community")
  //@UseCache({ttl: 60_000})
  @ContentType("application/json")
  async getCommunity() {
    return await this.prisma.communityRating.findMany();
  }

  @Get("/community/type/:id")
  @ContentType("application/json")
  async getCommunityForType(@PathParams("id") type_id: number) {
    return await this.prisma.communityRating.findMany({where: {type_id: type_id}});
  }

  @Get("/user")
  @ContentType("application/json")
  @UseAuth(AppUserAuthMiddleware)
  async getUser(@HeaderParams("x-user-id") userId: string){
    let ratingUsers = await this.prisma.ratingUser.findMany({where: {
      user_id: userId
    }, include: {rating: true}});
    return ratingUsers.map(ru => ru.rating);
  }

  @Post("/set")
  @ContentType("application/json")
  async set(@BodyParams() ratingSet: ReqRatingSet, @HeaderParams("x-user-id") userId?: string, @HeaderParams("x-secure-token") secureToken?: string){

    let id = ratingSet.id;

    let hasAuth = false;

    if(userId != null){
      let user = await this.prisma.user.findFirst({
        where: {
          id: userId,
          secureToken: secureToken
        }
      });
      hasAuth = user!= null;
      if(!hasAuth){
        throw new Forbidden("Authentification failed");
      }
    }

    if(id == null && userId != null){
      let cloud = await this.prisma.ratingUser.findFirst({where: {user_id: userId, rating: {type_id: ratingSet.type_id, criteria: ratingSet.criteria}}});
      if(cloud != null){
        id = cloud.rating_id;
      }
    }

    let rating: Rating | null = null;

    if(id != null && userId == null){
      let cloud = await this.prisma.ratingUser.findFirst({
        where: {
          rating_id: id,
        }
      });
      if(cloud != null){
        Sentry.captureMessage(`This rating ${id} are mapped with an user, but without app valid cloud-sync auth (userId: ${userId}, hasAuth=${hasAuth})`, 'warning');
        // Probably cloud-sync was disable, so we create a new rating entry
        id = null;
      }
    }

    if(id == null){
      rating = await this.prisma.rating.create({
        data: {
          note: ratingSet.note,
          type_id: ratingSet.type_id,
          criteria: ratingSet.criteria
        }
      });
      if(userId != null){
        let user = await this.prisma.user.findFirst({
          where: {
            id: userId,
            secureToken: secureToken
          }
        });
        if(user != null){
          await this.prisma.ratingUser.create({
            data: {
              rating_id: rating.id,
              user_id: user.id
            }
          });
        }
      }
    }else{
      rating = await this.prisma.rating.upsert({
        where: {
          id: id
        },
        update: {
          note: ratingSet.note
        },
        create: {
          type_id: ratingSet.type_id,
          criteria: ratingSet.criteria,
          note: ratingSet.note
        }
      });
    }
    return rating;
  }

  @Post("/link-cloud-sync")
  @ContentType("application/json")
  @UseAuth(AppUserAuthMiddleware)
  async linkUserRatingCloudSync(@BodyParams() data: ReqLinkCloudSync, @HeaderParams("x-user-id") userId: string){

    console.log(data);

    let ratings = await this.prisma.rating.findMany({
      where: {
        id: {in: data.ids}
      },
      include: {
        ratingUser: true
      }
    });

    let validRatings : Rating[] = [];

    for(let rating of ratings){
      let vRating : Rating = rating;
      if(rating.ratingUser?.user_id != null){
        if(rating.ratingUser.user_id != userId){
          //I don't know if this is necessary...
          vRating = await this.prisma.rating.create({
            data: {
              type_id: rating.type_id,
              criteria: rating.criteria,
              note: rating.note
            }
          });
        }
      }
      validRatings.push(vRating);
    }

    await this.prisma.ratingUser.createMany({
      data: validRatings.map(e => ({rating_id: e.id, user_id: userId})),
      skipDuplicates: true
    });

    return validRatings;

  }

  @Post("/unlink-cloud-sync")
  @ContentType("application/json")
  @UseAuth(AppUserAuthMiddleware)
  async unlinkUserRatingCloudSync(@BodyParams() data: ReqUnlinkCloudSync, @HeaderParams("x-user-id") userId: string){

    let userRatings = await this.prisma.ratingUser.findMany({
      where: {user_id: userId},
      include: { rating: true}
    });

    let ratings : Rating[] = userRatings.map(e => e.rating);

    if(data.recreateRating){
      ratings = await this.prisma.$transaction(ratings.map(e => this.prisma.rating.create({data: {type_id: e.type_id, criteria: e.criteria, note: e.note}})));
    }

    await this.prisma.ratingUser.deleteMany({
      where: {user_id: userId},
    });

    return ratings;
  }


}
