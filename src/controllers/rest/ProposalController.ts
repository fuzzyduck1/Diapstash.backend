import { Proposal, ProposalState } from "@prisma/client";
import { Controller, Inject } from "@tsed/di";
import { NotFound } from "@tsed/exceptions";
import { BodyParams, PathParams, QueryParams } from "@tsed/platform-params";
import { ContentType, Get, Post, Put } from "@tsed/schema";
import { Req } from "@tsed/common";
import { AuthRoles } from "../../decorators/authRoles";
import { JwtAuth } from "../../decorators/jwtAuth";
import { AuthUser } from "../../interface";
import { PrismaService } from "@tsed/prisma";

@Controller("/proposal")
export class ProposalController {

  @Inject()
  prisma: PrismaService;

  @Put("/user/submit")
  @ContentType("application/json")
  //@UseAuth(AppUserAuthMiddleware)
  async submit(@BodyParams() proposal: Proposal, @Req() req: Req) {
    delete (proposal as any)['id'];

    const userId = req.headers["x-user-id"] as string || null;
    if (userId != null) {
      proposal.userId = userId;
    }

    console.log(proposal);

    return await this.prisma.proposal.create({ data: proposal });
  }


  @Get("/many")
  @ContentType("application/json")
  //@UseAuth(AppUserAuthMiddleware)
  async getMany(@QueryParams("id", Number) ids: number[]) {
    return await this.prisma.proposal.findMany({ where: { id: { in: ids } } });
  }

  @Get("/:id")
  @ContentType("application/json")
  //@UseAuth(AppUserAuthMiddleware)
  async get(@PathParams("id") id: number) {
    if (id == null) {
      throw new NotFound("No proposal ask");
    }
    let proposal = await this.prisma.proposal.findFirst({ where: { id: id } });
    if (proposal == null) {
      throw new NotFound("No proposal found");
    }
    return proposal;
  }

  @Get("/")
  @ContentType("application/json")
  @JwtAuth()
  async getAll(@Req() req: Req) {

    let where = {};
    const isAdmin = ((req.user! as AuthUser).roles).includes('ADMIN');
    if (!isAdmin) {
      where = { hidden: false, state: { in: [ProposalState.APPROVED, ProposalState.WAITING] } };
    }

    return await this.prisma.proposal.findMany({
      select: {
        id: true,
        name: true,
        brand_code: true,
        brand: true,
        image: true,
        availableSizes: true,
        type: true,
        state: true,
        comment: true,
        reason: true,
        processProposals: {
          where: {
            processState: {
              in: [ProposalState.APPROVED, ProposalState.WAITING]
            }
          },
          select: {
            id: true,
            proposedType: {
              select: {
                id: true,
                name: true,
                brand: true,
                brand_code: true,
                availableSizes: true,
                type: true,
                image: true,
                target: true,
                style: true,
                discontinued: true
              }
            },
            proposedTypeId: true,
            state: true,
            processState: true,
            createdAt: true,
            updatedAt: true,
            source: true,
            user: false,
            username: false,
            typeProposal: {
              include: {
                originalType: true,
                newType: true,
              }
            },
            typeProposalId: true,
            duplicateProposal: true,
            duplicateProposalId: true
          }
        },
        user: isAdmin,
        userId: isAdmin,
        original_type_id: isAdmin,
        target: true,
        style: true,
        new_type_id: true,
        createdAt: true,
        updatedAt: true,
        hidden: isAdmin,
        internalNote: isAdmin
      },
      where: where,
      orderBy: { id: 'asc' }
    });
  }

  @Post("/")
  @ContentType("json")
  @JwtAuth()
  @AuthRoles('ADMIN')
  async update(@BodyParams() proposal: Proposal) {
    delete (proposal as any)['user'];
    delete (proposal as any)['brand'];
    delete (proposal as any)['processProposals'];
    delete (proposal as any)['duplicatedProposals'];
    return await this.prisma.proposal.update({
      where: { id: proposal.id },
      data: proposal
    })
  }

}
