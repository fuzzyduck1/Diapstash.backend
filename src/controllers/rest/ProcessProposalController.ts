import { ProcessProposal } from "@prisma/client";
import { Req } from "@tsed/common";
import {Controller, Inject} from "@tsed/di";
import { BodyParams, PathParams } from "@tsed/platform-params";
import {Get, Put, Post} from "@tsed/schema";
import { JwtAuth } from "../../decorators/jwtAuth";
import { AuthRoles } from "../../decorators/authRoles";
import { AuthUser } from "../../interface";
import { PrismaService } from "@tsed/prisma";


@Controller("/process-proposal")
@JwtAuth()
export class ProcessProposalController {

  @Inject()
  prisma: PrismaService;

  @Get("/:proposalId")
  async getByProposalId(@PathParams() proposalId: number) {

    return await this.prisma.processProposal.findMany({
      where: {
        proposalId: proposalId
      },
      select: {
        id: true,
        proposedType: true,
        proposedTypeId: true,
        state: true,
        createdAt: true,
        updatedAt: true,
        source: true,
        user: false,
        username: false
      },
    });
  }

  @Put('/submit')
  async submit(@Req() req: Req, @BodyParams() processProposal: ProcessProposal){
    processProposal.username = (req.user! as AuthUser).username as string;
    return await this.prisma.processProposal.create({data: processProposal});
  }

  @Post('/update')
  @AuthRoles('ADMIN')
  async update(@Req() req: Req, @BodyParams() processProposal: ProcessProposal){
    delete (processProposal as any)['proposal'];
    delete (processProposal as any)['proposedType'];
    delete (processProposal as any)['typeProposal'];
    delete (processProposal as any)['brandProposal'];
    delete (processProposal as any)['user'];
    delete (processProposal as any)['duplicateProposal'];
    delete (processProposal as any)['originalTypeProposal'];
    return await this.prisma.processProposal.update({ where: {id: processProposal.id},data: processProposal});
  }


}
