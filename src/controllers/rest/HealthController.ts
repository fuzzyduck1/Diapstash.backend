import {Controller, Inject} from "@tsed/di";
import { PrismaService } from "@tsed/prisma";
import {Get} from "@tsed/schema";

@Controller("/health")
export class HealthController {

  @Inject()
  prisma: PrismaService;

  @Get("/")
  async get() {
    await this.prisma.user.count();
    return {
      ok: true
    };
  }
}
