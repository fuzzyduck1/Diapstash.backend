import { AdminAccount, EType, DiaperStyle, DiaperTarget, User, ProposalOrigin, ProposalState  } from "@prisma/client";
import {Controller, Inject} from "@tsed/di";
import { BodyParams, PathParams, QueryParams } from "@tsed/platform-params";
import {ContentType, Get, Post, Put} from "@tsed/schema";
import { JwtAuth } from "../../decorators/jwtAuth";
import { AppJwtAuthOptional } from "../../decorators/appJwtAuth";
import { Req } from "@tsed/common";
import { NotFound, Unauthorized } from "@tsed/exceptions";
import { AuthRoles } from "../../decorators/authRoles";
import { UserRequest } from "../../interface";
import { AppAuthOptional } from "../../decorators/appAuth";
import { PrismaService } from "@tsed/prisma";

interface SubmitType {
  comment: string;
  originalTypeId?: number;
  name: string;
  brand_code: string | null;
  image: string | null;
  availableSizes: string[];
  type: EType;
  style: DiaperStyle | null;
  target: DiaperTarget[];
  discontinued: boolean;
}

interface SubmitBrand {
  comment: string;
  originalBrandCode?: string;
  name: string;
  image: string | null;
}

interface SubmitTypeProposal {
  typeProposalId: string;
  comment: string;
  jsonPath: string;
  state: ProposalState;
}

interface SubmitBrandProposal {
  brandProposalId: string;
  comment: string;
  jsonPath: string;
  state: ProposalState;
}

interface PatchProposal {
  id: string;
  state?: ProposalState;
}

interface PatchTypeProcessProposal {
  id: string;
  state?: ProposalState;
}

interface PatchBrandProcessProposal {
  id: string;
  state?: ProposalState;
}

interface PatchTypeProposal {
  id: string;
  originalTypeId?: number;
  resultTypeId?: number;
}

interface PatchBrandProposal {
  id: string;
  originalBrandCode?: string;
  resultBrandCode?: string;
}

interface ReqGetMany {
  ids: string[]
}

@Controller("/v2/proposal")
export class ProposalV2Controller {

  @Inject()
  prisma: PrismaService;

  @Get("/all")
  @JwtAuth()
  @ContentType("application/json")
  async getAll() {

    return this.prisma.proposalV2.findMany({
      where: {
        state: {in: [ProposalState.WAITING]}
      },
      select: {
        id: true,
        proposalOrigin: true,
        state: true,
        appUser: false,
        appUserId: false,
        adminUsername: false,
        adminUser: false,
        createdAt: true,
        updatedAt: true,
        typeProposal: {
          select: {
            id: true,
            proposalId: true,
            proposal: false,
            originalTypeId: true,
            originalType: {
              where: {official: true}
            },

            resultTypeId: true,
            resultType: true,


            name: true,
            brand: true,
            brand_code: true,
            availableSizes: true,
            type: true,
            image: true,
            target: true,
            style: true,
            discontinued: true,

            comment: true,
            reason: true,

            createdAt: true,
            updatedAt: true,
            processProposal: {
              select: {
                id: true,
                typeProposalId: true,
                typeProposal: false,
                originalType: true,
                originalTypeId: true,
                jsonPath: true,
                comment: true,
                state: true,
                username: false,
                user: false,
                createdAt: true,
                updatedAt: true
              },
              where: {
                state: {in: [ProposalState.APPROVED, ProposalState.WAITING]}
              }
            }
          }
        },
        brandProposal: {
          select: {
            id: true,
            proposalId: true,
            proposal: false,
            originalBrand: true,
            originalBrandCode: true,
            resultBrand: true,
            resultBrandCode: true,
            name: true,
            image: true,
            comment: true,
            processProposal: {
              select: {
                id: true,
                brandProposalId: true,
                brandProposal: false,
                originalBrand: true,
                originalBrandCode: true,
                jsonPath: true,
                comment: true,
                state: true,
                username: false,
                user: false,
                createdAt: true,
                updatedAt: true
              },
              where: {
                state: {in: [ProposalState.APPROVED, ProposalState.WAITING]}
              }
            },
            createdAt: true,
            updatedAt: true
          }
        }
      }
    })
  }


  @Get("/:id")
  @ContentType("application/json")
  @AppJwtAuthOptional()
  async getId(@PathParams("id") id: string) {
    let p = await this.prisma.proposalV2.findFirst({
      where: {
        id: id
      },
      select: {
        id: true,
        proposalOrigin: true,
        state: true,
        appUser: false,
        appUserId: false,
        adminUsername: false,
        adminUser: false,
        createdAt: true,
        updatedAt: true,
        typeProposal: {
          select: {
            id: true,
            proposalId: true,
            proposal: false,
            originalTypeId: true,
            originalType: {
              where: {official: true}
            },

            resultTypeId: true,
            resultType: true,

            name: true,
            brand: true,
            brand_code: true,
            availableSizes: true,
            type: true,
            image: true,
            target: true,
            style: true,
            discontinued: true,

            comment: true,
            reason: true,

            createdAt: true,
            updatedAt: true,
            processProposal: {
              select: {
                id: true,
                typeProposalId: true,
                typeProposal: false,
                originalTypeId: true,
                originalType: true,
                jsonPath: true,
                comment: true,
                state: true,
                username: false,
                user: false,
                createdAt: true,
                updatedAt: true
              },
              where: {
                state: {in: [ProposalState.APPROVED, ProposalState.WAITING]}
              }
            }
          }
        },
        brandProposal: {
          select: {
            id: true,
            proposalId: true,
            proposal: false,
            originalBrand: true,
            originalBrandCode: true,
            resultBrand: true,
            resultBrandCode: true,
            name: true,
            image: true,
            comment: true,
            processProposal: {
              select: {
                id: true,
                brandProposalId: true,
                brandProposal: false,
                originalBrandCode: true,
                originalBrand: true,
                jsonPath: true,
                comment: true,
                state: true,
                username: false,
                user: false,
                createdAt: true,
                updatedAt: true
              },
              where: {
                state: {in: [ProposalState.APPROVED, ProposalState.WAITING]}
              }
            },
            createdAt: true,
            updatedAt: true
          }
        }
      }
    });

    if(p == null){
      throw new NotFound("proposal not found");
    }

    return p;
  }

  @Post('/many')
  @ContentType('application/json')
  @AppAuthOptional()
  async getMany(@Req() req: UserRequest, @BodyParams() body: ReqGetMany){
    let where : any = {
      id: {in: body.ids},
      typeProposal: {isNot: null},
    };
    if(req.user != null){
      where["appUserId"] = (req.user! as User).id;
    }

    return this.prisma.proposalV2.findMany({
      where: where,
      select: {
        id: true,
        proposalOrigin: true,
        state: true,
        appUser: false,
        appUserId: false,
        adminUsername: false,
        adminUser: false,
        createdAt: true,
        updatedAt: true,
        typeProposal: {
          select: {
            id: true,
            proposalId: true,
            proposal: false,
            originalTypeId: true,
            originalType: true,
            resultTypeId: true,
            resultType: true,
            name: true,
            brand: true,
            brand_code: true,
            availableSizes: true,
            type: true,
            image: true,
            target: true,
            style: true,
            discontinued: true,
            comment: true,
            reason: true,
            createdAt: true,
            updatedAt: true,
            processProposal: false
          }
        }
      }
    })
  }

  @Post("/patch")
  @JwtAuth()
  @AuthRoles('ADMIN')
  async patchProposal(@BodyParams() data: PatchProposal){
    return this.prisma.proposalV2.update({
      where: {
        id: data.id
      },
      data: data
    });
  }


  @Put("/submit/type")
  @AppJwtAuthOptional()
  async submitType(@QueryParams("from") from: string, @Req() req: Req,@BodyParams() proposal: SubmitType){

    let origin = from == "catalog" ? ProposalOrigin.CATALOG : ProposalOrigin.APP;


    let user = req.user ?? {};
    let userIsApp = (user as any)['id'] != null;
    let userIsJWT = (user as any)['username'] != null;

    let adminUsername : string | null = null;
    let userId: string | null = null;

    console.log("origin", origin);

    if(origin == ProposalOrigin.CATALOG){
      if(userIsJWT){
        adminUsername = (user as AdminAccount).username;
      }else{
        throw new Unauthorized("origin catalog cannot be set from app");
      }
    }else{
      if(userIsApp){
        let appUser = user as User;
        userId = appUser.id;
      }
      //if no user in app = sync-cloud disable
    }

    let originalTypeId : undefined | number = proposal.originalTypeId;
    if(originalTypeId != null && originalTypeId < 0){
      originalTypeId = undefined;
    }

    let tpc = {
      comment: proposal.comment,
      name: proposal.name,
      image: proposal.image,
      availableSizes: proposal.availableSizes,
      type: proposal.type,
      style: proposal.style,
      target: proposal.target,
      discontinued: proposal.discontinued,
      originalTypeId: originalTypeId,
      brand_code: proposal.brand_code
    }

    return await this.prisma.proposalV2.create({
      data: {
        proposalOrigin: origin,
        adminUsername: adminUsername,
        appUserId: userId,
        state: ProposalState.WAITING,
        typeProposal: {
          create: tpc
        }
      }
    })

  }

  @Put("/submit/brand")
  @JwtAuth()
  async submitBrand(@QueryParams("from") from: string, @Req() req: Req,@BodyParams() proposal: SubmitBrand){

    let origin = from == "catalog" ? ProposalOrigin.CATALOG : ProposalOrigin.APP;

    let userIsApp = (req.user as any)['id'] != null;
    let userIsJWT = (req.user as any)['username'] != null;

    let adminUsername : string | null = null;
    let userId: string | null = null;

    if(origin == ProposalOrigin.CATALOG){
      if(userIsJWT){
      adminUsername = (req.user as AdminAccount).username;
      }else{
        throw new Unauthorized("origin catalog cannot be set from app");
      }
    }else{
      // if(userIsApp){
      //   let user = req.user as User;
      //   userId = user.id;
      // }else{
      //   throw new Unauthorized("app catalog cannot be set with jwt");
      // }
      throw new Unauthorized("Brand must be created from /catalog");
    }

    return await this.prisma.proposalV2.create({
      data: {
        proposalOrigin: origin,
        adminUsername: adminUsername,
        appUserId: userId,
        state: ProposalState.WAITING,
        brandProposal: {
          create: {
            ...proposal
          }
        }
      }
    })

  }

  @Put("/submit/type-process-proposal")
  @JwtAuth()
  async submitTypeProcessProposal(@Req() req: Req, @BodyParams() tpp: SubmitTypeProposal){
    return await this.prisma.typeProcessProposal.create({
      data: {
        username: (req.user as AdminAccount).username,
        ...tpp
      }
    });
  }

  @Put("/submit/brand-process-proposal")
  @JwtAuth()
  async submitBrandProcessProposal(@Req() req: Req, @BodyParams() bpp: SubmitBrandProposal){
    return await this.prisma.brandProcessProposal.create({
      data: {
        username: (req.user as AdminAccount).username,
        ...bpp
      }
    });
  }

  @Post('/type-process-proposal/patch')
  @JwtAuth()
  async patchTypeProcesssProposal(@BodyParams() data: PatchTypeProcessProposal){
    return this.prisma.typeProcessProposal.update({
      where: {
        id: data.id
      },
      data: data
    });
  }

  @Post('/brand-process-proposal/patch')
  @JwtAuth()
  async patchBrandProcesssProposal(@BodyParams() data: PatchBrandProcessProposal){
    return this.prisma.brandProcessProposal.update({
      where: {
        id: data.id
      },
      data: data
    });
  }

  @Post('/type-proposal/patch')
  @JwtAuth()
  async patchTypeProposal(@BodyParams() data: PatchTypeProposal){
    console.log(data);
    return this.prisma.typeProposalV2.update({
      where: {
        id: data.id
      },
      data: data
    });
  }

  @Post('/brand-proposal/patch')
  @JwtAuth()
  async patchBrandProposal(@BodyParams() data: PatchBrandProposal){
    return this.prisma.brandProposalV2.update({
      where: {
        id: data.id
      },
      data: data
    });
  }
}
