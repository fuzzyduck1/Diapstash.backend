import { Controller, Inject } from "@tsed/di";
import { Delete, Get, Post, Put } from "@tsed/schema";

import { AppUserAuthMiddleware } from "../../middlewares/AppUserAuthMiddleware";
import { UseAuth } from "@tsed/platform-middlewares";
import { BodyParams, HeaderParams } from "@tsed/platform-params";
import { PrismaService } from "@tsed/prisma";

@Controller("/favorites")
export class FavoritesController {

  @Inject()
  prisma: PrismaService;

  @Get("/")
  @UseAuth(AppUserAuthMiddleware)
  async get(@HeaderParams("x-user-id") userId: string) {
    const fav = await this.prisma.favoriteType.findMany({
      where: {
        user_id: userId
      },
      include: {
        type: true
      }
    });
    return fav.map(f => f.type);
  }

  @Put("/")
  @UseAuth(AppUserAuthMiddleware)
  async add(@HeaderParams("x-user-id") userId: string, @BodyParams("type_id") typeId: number) {
    return await this.prisma.favoriteType.upsert({
      create: {
        user_id: userId,
        type_id: typeId
      },
      update: {},
      where: {
        user_id_type_id: {
          user_id: userId,
          type_id: typeId
        }
      }
    });
  }

  @Post("/")
  @UseAuth(AppUserAuthMiddleware)
  async addAll(@HeaderParams("x-user-id") userId: string, @BodyParams() typesId: number[]) {
    let res = [];
    await this.prisma.favoriteType.deleteMany({
      where: {
        user_id: userId
      }
    });

    for (let id of typesId) {
      res.push(await this.prisma.favoriteType.create({
        data: {
          user_id: userId,
          type_id: id
        }
      }));
    }
    return res;
  }

  @Delete("/")
  @UseAuth(AppUserAuthMiddleware)
  async remove(@HeaderParams("x-user-id") userId: string, @BodyParams("type_id") typeId: number) {
    return await this.prisma.favoriteType.delete({
      where: {
        user_id_type_id: {
          user_id: userId,
          type_id: typeId
        }
      }
    });
  }

}
