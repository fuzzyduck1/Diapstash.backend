import { AdminAccount } from "@prisma/client";
import { Controller, Inject } from "@tsed/di";
import { Unauthorized } from "@tsed/exceptions";
import { BodyParams, Context } from "@tsed/platform-params";
import { Post } from "@tsed/schema";
import { JWT_SECRET } from "../../config/envs";
import * as bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { UseAuth } from "@tsed/common";
import { AdminAuthMiddleware } from "../../middlewares/AdminAuthMiddleware";
import { PrismaService } from "@tsed/prisma";

@Controller("/authenticate")
export class AuthenticateController {

  @Inject()
  prisma: PrismaService;


  buildToken(user: AdminAccount){
    let token = jwt.sign({
      username: user.username,
      requireChangePassword: user.requireChangePassword
    }, JWT_SECRET, {
      expiresIn: "1h"
    });

    return token;
  }

  @Post("/login")
  async login(@BodyParams('username') username: string, @BodyParams('password') password: string) {

    let user = await this.prisma.adminAccount.findFirst({
      where: {
        username: username
      }
    });

    if (user == null) {
      throw new Unauthorized('No user found or password invalid');
    }


    if (!await bcrypt.compare(password, user.password)) {
      throw new Unauthorized('No user found or password invalid.');
    }

    let token = this.buildToken(user);


    return token;
  }

  @Post("/password")
  @UseAuth(AdminAuthMiddleware)
  async password(@BodyParams('password') password: string, @Context('user') user: AdminAccount) {


    let encryptedPassword = await bcrypt.hash(password, 10);

    let newUser = await this.prisma.adminAccount.update({
      where: { username: user.username },
      data: {
        password: encryptedPassword,
        requireChangePassword: false
      }
    });

    let token = this.buildToken(newUser);

    return token;
  }



}
