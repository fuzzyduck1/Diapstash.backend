import { AdminAccount } from "@prisma/client";
import { Controller, Inject } from "@tsed/di";
import { BodyParams, Context, HeaderParams } from "@tsed/platform-params";
import { Get, Post } from "@tsed/schema";
import * as bcrypt from "bcrypt";
import { Req } from "@tsed/common";
import { Authenticate } from "@tsed/passport";
import { AuthRoles } from "../../decorators/authRoles";
import { JwtAuth } from "../../decorators/jwtAuth";
import { buildToken } from "../../protocols/LocalProtocol";
import jwt, { JwtPayload } from "jsonwebtoken";
import { REFRESH_SECRET } from "../../config/envs";
import { AuthUser, UserRequest } from "../../interface";
import { PrismaService } from "@tsed/prisma";

@Controller("/auth2")
export class Auth2Controller {

  @Inject()
  prisma: PrismaService;

  @Post("/login")
  @Authenticate("local")
  async login(@Req() req: Req, @BodyParams('username') username: string, @BodyParams('password') password: string) {
    console.log('login', req.user);
    return req.user;
  }

  @Get("/roles")
  @JwtAuth()
  async getRoles(@Req() req: UserRequest) {
    let user = await this.prisma.adminAccount.findFirst({where: {username: (req.user! as AuthUser).username}});

    return {roles: user?.roles ?? []};
  }

  @Post("/register")
  // @Authenticate("local")
  async register(@Req() req: Req, @BodyParams('email') email: string, @BodyParams('username') username: string, @BodyParams('password') password: string) {
    let user = await this.prisma.adminAccount.findFirst({
      where: {
        OR: [
          {username: username},
          {email: email}
        ]
      }
    });
    if(user != null){
      return {
        messageI18N: 'register.already-account',
        error: true,
      };
    }

    if(password.length < 7){
      return {
        error: true,
        messageI18N: 'register.required-password-length'
      }
    }

    let encryptedPassword = await bcrypt.hash(password, 10);

    await this.prisma.adminAccount.create({
      data: {
        username: username,
        email: email,
        password: encryptedPassword,
        requireChangePassword: false,
        roles: ["USER"]
      }
    });

    return {
      error: false,
      messageI18N: 'register.account-created'
    }
  }

  @Post("/password")
  @JwtAuth()
  @AuthRoles('ADMIN')
  async password(@BodyParams('password') password: string, @Context('user') user: AdminAccount) {


    let encryptedPassword = await bcrypt.hash(password, 10);

    let newUser = await this.prisma.adminAccount.update({
      where: { username: user.username },
      data: {
        password: encryptedPassword,
        requireChangePassword: false
      }
    });

    let token = buildToken(newUser);

    return token;
  }

  @Post("/refresh")
  async refresh(@HeaderParams('authorization') auth: string, @Context('user') user: AdminAccount) {

    let refreshToken = auth.replace("Bearer", "").trim();


    var checkedToken = jwt.verify(refreshToken, REFRESH_SECRET);

    if(checkedToken){
      let username = (checkedToken as JwtPayload).username;
      let user = await this.prisma.adminAccount.findFirst({where: {username: username}});
      if(user){
        let token = buildToken(user);
        return {token: token};
      }
    }

    return {};
  }


}
