import { Controller, Inject } from "@tsed/di";
import { QueryParams } from "@tsed/platform-params";
import { ContentType, Get } from "@tsed/schema";
import { CrowdinService } from "../../services/CrowdinService";
import { PrismaService } from "@tsed/prisma";


@Controller("/whats-new")
export class WhatsNewController {

  cacheStrings = {};

  @Inject()
  prisma: PrismaService;

  @Inject()
  crowdin: CrowdinService;

  async parseLogs(logs: any[]) {
    let logsTranslated: any[] = [];

    logsTranslated = logs.map(l => {
      let messages : {[key: string]: {[key: string]: string}} = {};
      for(let msg of l.messages){
        messages[msg.lang] = {
          title: msg.title,
          description: msg.description,
        }
      }
      return {
        ...l,
        messages: messages
      }
    });

    return logsTranslated;
  }

  @Get("/")
  @ContentType("application/json")
  async get(@QueryParams('from') from: number, @QueryParams('to') to: number) {
    let logs = await this.prisma.whatsNews.findMany({
      where: {
        AND: [
          { buildCode: { gt: from } },
          { buildCode: { lte: to } }
        ]
      },
      orderBy: [{ buildCode: 'asc' }, { id: 'asc' }],
      include: {messages: true}
    });

    return await this.parseLogs(logs);
  }

  @Get("/last")
  @ContentType("application/json")
  async getLast(@QueryParams('max') max?: number) {
    let lastLogs = await this.prisma.whatsNews.findFirst({
      orderBy: [{ buildCode: 'desc' }, { id: 'desc' }],
      where: { buildCode: { lte: (max ?? 500) } }
    });
    let logs = await this.prisma.whatsNews.findMany({
      where: {
        buildCode: lastLogs?.buildCode
      },
      orderBy: [{ buildCode: 'asc' }, { id: 'asc' }],
      include: {messages: true}
    });

    return await this.parseLogs(logs);
  }

  @Get("/load")
  @ContentType("application/json")
  async load() {
    const res = await this.crowdin.runCheck();
    return {ok: res};
  }
}
