import { ProposalState, BrandProposal } from "@prisma/client";
import {Controller, Inject} from "@tsed/di";
import { BodyParams } from "@tsed/platform-params";
import {Get, Put, Post} from "@tsed/schema";
import { JwtAuth } from "../../decorators/jwtAuth";
import { AuthRoles } from "../../decorators/authRoles";
import { AuthUser, UserRequest } from "../../interface";
import { Req } from "@tsed/common";
import { PrismaService } from "@tsed/prisma";

@Controller("/brand-proposal")
@JwtAuth()
export class BrandProposalController {


  @Inject()
  prisma: PrismaService;


  @Get('/without-proposal')
  async getWithoutProposal(){
    return await this.prisma.brandProposal.findMany({
      where: {
        proposalId: null,
        state: ProposalState.WAITING
      },
      include: {
        originalBrand: true
      }
    })
  }

  @Put("/submit")
  async submit(@Req() req: UserRequest, @BodyParams() brandProposal: BrandProposal){
    brandProposal.state = ProposalState.WAITING;
    brandProposal.username = (req.user as AuthUser)?.username ?? null;
    return await this.prisma.brandProposal.create({data: (brandProposal as any)});
  }

  @Post("/update")
  @AuthRoles('ADMIN')
  async update(@BodyParams() brandProposal: BrandProposal){
    delete (brandProposal as any)['proposal'];
    delete (brandProposal as any)['originalBrand'];
    delete (brandProposal as any)['user'];
    return await this.prisma.brandProposal.update({where: {id: brandProposal.id}, data: (brandProposal as any)});
  }


}
