import { SharedStock, SharedStockAccess, SharedStockState } from "@prisma/client";
import { Controller, Inject } from "@tsed/di";
import { UseAuth } from "@tsed/platform-middlewares";
import { BodyParams, HeaderParams } from "@tsed/platform-params";
import { Get, Post, Put } from "@tsed/schema";
import { AppUserAuthMiddleware } from "../../middlewares/AppUserAuthMiddleware";
import { randomUUID } from "crypto";
import { NotFound, Unauthorized } from "@tsed/exceptions";
import { PrismaService } from "@tsed/prisma";

@Controller("/shared-stock")
export class SharedStockController {

  @Inject()
  prisma: PrismaService;


  @Put("/generate")
  @UseAuth(AppUserAuthMiddleware)
  async generateSharing(@BodyParams("toName") name: string, @HeaderParams("x-user-id") userId: string, @BodyParams("readonly") readonly?: boolean) {
    let securityToken = randomUUID().replace(/-/g, "");
    let sh = await this.prisma.sharedStock.create({
      data: {
        toName: name,
        userId: userId,
        securityToken: securityToken,
        state: SharedStockState.PENDING,
        readonly: readonly
      }
    });

    return sh;
  }

  @Post("/revoke")
  //@UseAuth(AppUserAuthMiddleware)
  async revoke(@BodyParams("id") id: number, @BodyParams("securityToken") securityToken: string) {
    await this.prisma.sharedStock.deleteMany({
      where: {
        id: id,
        securityToken: securityToken
      }
    });

    return { ok: true };
  }


  @Post("/update")
  @UseAuth(AppUserAuthMiddleware)
  async update(@HeaderParams("x-user-id") userId: string, @BodyParams() ss: SharedStock) {

    let found = await this.prisma.sharedStock.findFirst({
      where: { id: ss.id }
    });

    if (found) {
      if (found.userId == userId) {
        return await this.prisma.sharedStock.update({
          where: { id: ss.id },
          data: ss
        });
      } else {
        throw new Unauthorized("User not allowed to edit this");
      }

    } else {
      throw new NotFound("");
    }
  }

  @Get("/list/mines")
  @UseAuth(AppUserAuthMiddleware)
  async getSharedStock(@HeaderParams("x-user-id") userId: string) {
    return await this.prisma.sharedStock.findMany({
      where: { userId: userId },
      select: { id: true, toName: true, state: true, securityToken: true, fromName: false, user: false, sharedStockAccess: false, userId: false, readonly: true }
    });
  }

  @Get("/list/access")
  @UseAuth(AppUserAuthMiddleware)
  async getSharedAccess(@HeaderParams("x-user-id") userId: string) {
    return await this.prisma.sharedStockAccess.findMany({
      where: { userId: userId },
      select: {
        user: false, userId: false,
        sharedStock: {
          select: {
            id: true,
            securityToken: true,
            toName: false,
            fromName: true,
            user: false,
            userId: false,
            state: true,
            readonly: true
          }
        }
      }
    });
  }


  @Post("/stock/use")
  async useSharedStock(@BodyParams("id") ssId: number, @BodyParams("securityToken") securityToken: string, @BodyParams("type_id") typeId: number, @BodyParams("size") size: string) {


    let ss = await this.prisma.sharedStock.findFirst({
      where: { id: ssId, securityToken: securityToken }
    });
    if (ss == null) {
      throw new NotFound("");
    }

    if(ss.readonly){
      throw new Unauthorized("read only");
    }

    let stock = await this.prisma.diaperStock.findFirst({
      where: { userId: ss.userId, diaperTypeId: typeId }
    });

    if (stock == null) {
      throw new NotFound("");
    }

    let counts = stock.counts as any;

    if (counts[size] != null) {
      if (counts[size] > 0) {
        counts[size] -= 1;
        if (counts[size] <= 0) {
          delete counts[size];
        }
        stock.counts = counts;

        let stockSaved =  await this.prisma.diaperStock.update({
          where: {
            diaperTypeId_userId: {
              userId: stock.userId,
              diaperTypeId: stock.diaperTypeId
            },
          },
          data: {
            counts: counts
          }
        });
        return {ok: true, stock: stockSaved}
      }
    }
    return {ok: false};
  }


  @Post("/link/access")
  //@UseAuth(AppUserAuthMiddleware)
  async linkAccess(@BodyParams("id") id: number, @BodyParams("securityToken") securityToken: string, @BodyParams("fromName") fromName: string, @HeaderParams("x-user-id") userId?: string) {
    let sh = await this.prisma.sharedStock.findFirst({
      where: { id: id, securityToken: securityToken }
    });
    if (sh == null) {
      throw new NotFound("");
    }

    if (sh.state != SharedStockState.PENDING) {
      return { ok: false, err_code: 'already_linked' }
    }

    sh.fromName = fromName;
    sh.state = SharedStockState.ACCEPTED;

    await this.prisma.sharedStock.update({ data: sh, where: { id: sh.id } });

    var ssa: SharedStockAccess | null = null;

    if (userId != null) {
      ssa = await this.prisma.sharedStockAccess.create({
        data: {
          sharedStockId: sh.id,
          userId: userId
        }
      })
    }

    return { ok: sh != null, ssa: ssa, sh_id: sh.id, readonly: sh.readonly };
  }

  @Post("/validate/access")
  //@UseAuth(AppUserAuthMiddleware)
  async validateAccess(@BodyParams("id") id: number, @BodyParams("securityToken") securityToken: string) {
    let sh = await this.prisma.sharedStock.findFirst({
      where: { id: id, securityToken: securityToken }
    });
    return { ok: sh != null, sharedstock: sh };
  }

  @Post("/stock/lookup")
  async getStock(@BodyParams("id") id: number, @BodyParams("securityToken") securityToken: string) {
    let stock = await this.prisma.sharedStock.findFirst({
      where: { id: id, securityToken: securityToken },
      select: {
        fromName: true,
        readonly: true,
        user: {
          select: {
            id: false,
            name: false,
            secureToken: false,
            changes: false,
            stocks: {
              select: {
                counts: true,
                diaperType: true,
                diaperTypeId: true,
                user: false,
                userId: false,
                prices: true
              }
            },
            proposals: false,
            sharedHistories: false,
            sharedStocks: false,
            customTypes: false,
            sharedHistoriesAccess: false,
            sharedStockAccess: false
          }
        }
      }
    });
    if (stock == null) {
      throw new NotFound("");
    }
    return stock;
  }

}
