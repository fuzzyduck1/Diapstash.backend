import {Controller, Inject} from "@tsed/di";
import {Get} from "@tsed/schema";
import { AuthRoles } from "../../decorators/authRoles";
import { JwtAuth } from "../../decorators/jwtAuth";
import { PrismaService } from "@tsed/prisma";

@Controller("/stats")

export class StatsController {

  @Inject()
  prisma: PrismaService;

  @Get("/landing")
  async landing() {
    return {
      brands: await this.prisma.diaperBrand.count(),
      types: await this.prisma.diaperType.count({where: {official: true, hidden: false}}),
    }
  }

  @Get("/main")
  @JwtAuth()
  @AuthRoles('ADMIN')
  async get() {
    let stocks = await this.prisma.diaperStock.findMany();
    let diapersInStock = 0;
    for(let stock of stocks){
      for(let count of Object.values(stock.counts as {[key: string]: number})){
        if(count < 50_000){
          diapersInStock += count;
        }
      }
    }

    const rawQueryDurationChange = `
    select extract(epoch from sum("endTime" - "startTime"))::int sumDuration, extract(epoch from avg("endTime" - "startTime"))::int avgDuration
    from "Change"
    where "endTime" is not null
    and "endTime" > "startTime"
    and "endTime" - "startTime" < '3 days'
    `;

    const rawQueryUserActiveMonth = `select count(distinct "userId")::int as c from "DiaperStock" where (now() - "updatedAt") <= '1 month';`;
    const rawQueryUserActiveWeek = `select count(distinct "userId")::int as c from "DiaperStock" where (now() - "updatedAt") <= '1 week';`;
    const rawQueryUserActiveDay = `select count(distinct "userId")::int as c from "DiaperStock" where (now() - "updatedAt") <= '1 day';`;

    const changeDuration: any[] = await this.prisma.$queryRawUnsafe(rawQueryDurationChange);

    const usersActiveMonth = (await this.prisma.$queryRawUnsafe(rawQueryUserActiveMonth) as any)[0].c
    const usersActiveWeek = (await this.prisma.$queryRawUnsafe(rawQueryUserActiveWeek) as any)[0].c
    const usersActiveDay = (await this.prisma.$queryRawUnsafe(rawQueryUserActiveDay) as any)[0].c


    return {
      users: await this.prisma.user.count(),
      brands: await this.prisma.diaperBrand.count(),
      types: await this.prisma.diaperType.count(),
      officialTypes: await this.prisma.diaperType.count({where: {official: true}}),
      customTypes: await this.prisma.diaperType.count({where: {official: false}}),
      changes: await this.prisma.change.count(),
      currentChanges: await this.prisma.change.count({where: {  endTime: null }}),
      diapersInStock: diapersInStock,
      changeDuration: changeDuration[0],
      usersActiveMonth, usersActiveWeek, usersActiveDay
    };
  }
}
