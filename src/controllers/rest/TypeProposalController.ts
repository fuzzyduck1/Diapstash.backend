import { ProposalState, TypeProposal } from "@prisma/client";
import {Controller, Inject} from "@tsed/di";
import { BodyParams } from "@tsed/platform-params";
import {Get, Put, Post } from "@tsed/schema";
import { JwtAuth } from "../../decorators/jwtAuth";
import { AuthRoles } from "../../decorators/authRoles";
import { Req } from "@tsed/common";
import { AuthUser, UserRequest } from "../../interface";
import { PrismaService } from "@tsed/prisma";

@Controller("/type-proposal")
@JwtAuth()
export class TypeProposalController {


  @Inject()
  prisma: PrismaService;


  @Get('/without-proposal')
  async getWithoutProposal(){
    let res =  await this.prisma.typeProposal.findMany({
      where: {
        proposalId: null,
        originTypeProposalId: null,
        state: {in: [ProposalState.WAITING]}
      },
      include: {
        originalType: {
          include: {
            brand: true
          }
        },
        newType: true,
        typeProcessProposals: {
          include: {
            originalTypeProposal: true,
            typeProposal: {
              include: {
                newType: true
              }
            }
          }
        }
      }
    });
    return res;
  }

  @Put("/submit")
  async submit(@Req() req: UserRequest, @BodyParams() typeProposal: TypeProposal){
    typeProposal.state = ProposalState.WAITING;
    typeProposal.username = (req.user as AuthUser)?.username ?? null;
    return await this.prisma.typeProposal.create({data: (typeProposal as any)});
  }

  @Post("/update")
  @AuthRoles('ADMIN')
  async update(@BodyParams() typeProposal: TypeProposal){
    delete (typeProposal as any)['proposal'];
    delete (typeProposal as any)['originalType'];
    delete (typeProposal as any)['newType'];
    delete (typeProposal as any)['user'];
    delete (typeProposal as any)['typeProcessProposals'];
    return await this.prisma.typeProposal.update({where: {id: typeProposal.id}, data: (typeProposal as any)});
  }
}
