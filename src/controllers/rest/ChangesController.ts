import { Change, DiaperChange } from "@prisma/client";
import { Req } from "@tsed/common";
import { Controller, Inject } from "@tsed/di";
import { UseAuth } from "@tsed/platform-middlewares";
import { BodyParams, HeaderParams } from "@tsed/platform-params";
import { ContentType, Delete, Get, Post } from "@tsed/schema";
import { AppUserAuthMiddleware } from "../../middlewares/AppUserAuthMiddleware";
import { PrismaService } from "@tsed/prisma";

interface IPostChange extends Change {
  diapers: DiaperChange[];
}

interface IPostChanges {
  changes: IPostChange[];
}

interface IDiaperCount {
  diaper: DiaperChange,
  count: number
};

@Controller("/changes")
export class ChangesController {

  @Inject()
  prisma: PrismaService;

  @Get("/")
  @ContentType("application/json")
  @UseAuth(AppUserAuthMiddleware)
  async getChanges(@Req() req: Req){
    const userId = req.headers["x-user-id"] as string;

    let changes = await this.prisma.change.findMany({
      where: {
        user_id: userId
      },
      include: {diapers: true}
    });
    return changes;
  }

  @Delete("/")
  @ContentType("application/json")
  @UseAuth(AppUserAuthMiddleware)
  async deleteChange(@BodyParams() change: IPostChange) {
    await this.prisma.change.delete({
      where: {
        id_user_id: {
          id: change.id,
          user_id: change.user_id
        }
      },
      include: {diapers: true}
    });

  }

  @Post("/clear")
  @ContentType("application/json")
  @UseAuth(AppUserAuthMiddleware)
  async clearChange(@HeaderParams("x-user-id") userId: string) {
    await this.prisma.diaperChange.deleteMany({
      where: {
        user_id: userId
      }
    })
    await this.prisma.change.deleteMany({
      where: {
        user_id: userId
      },
    });

    return  {ok: true};
  }

  @Post("/")
  @ContentType("application/json")
  @UseAuth(AppUserAuthMiddleware)
  async updateChange(@BodyParams() data: IPostChanges) {
    let postChanges = data.changes;
    for (let postChange of postChanges) {
      let change = postChange as Change;

      await this.prisma.change.upsert({
        where: {
          id_user_id: {
            id: change.id,
            user_id: change.user_id
          }
        },
        update: {
          startTime: new Date(change.startTime),
          endTime: change.endTime != null ? new Date(change.endTime) : null,
          state: change.state,
          note: change.note,
          tags: change.tags || [],
          wetness: change.wetness,
          price: change.price
        },
        create: {
          id: change.id,
          user_id: change.user_id,
          startTime: new Date(change.startTime),
          endTime: change.endTime != null ? new Date(change.endTime) : null,
          tags: change.tags || [],
          wetness: change.wetness,
          state: change.state,
          note: change.note,
          price: change.price
        }
      });

      let diapers = await this.prisma.diaperChange.findMany({
        where: {
          change_id: change.id,
          user_id: change.user_id
        },
        orderBy: {
          order: 'asc'
        }
      });

      let diapersToSaveWithCount: IDiaperCount[] = [];

      for(let i = 0; i < postChange.diapers.length; i++){
        let diaper = postChange.diapers[i];



        if(i >= diapers.length){
          await this.prisma.diaperChange.create({
            data: {
              change_id: change.id,
              user_id: change.user_id,
              type_id: diaper.type_id,
              size: diaper.size,
              count: 1,
              order: i+1,
              price: diaper.price
            }
          });
        }else{
          let savedDiaper = diapers[i];
          if(savedDiaper.type_id != diaper.type_id || savedDiaper.size != diaper.size || savedDiaper.price != diaper.price || savedDiaper.order != diaper.order){
            await this.prisma.diaperChange.update({
              where: {
                change_id_user_id_order:{
                  change_id: change.id,
                  user_id: change.user_id,
                  order: savedDiaper.order
                }
              },
              data: {
                type_id: diaper.type_id,
                size: diaper.size,
                count: 1,
                price: diaper.price
              }
            });
          }
        }
      }

      if(diapers.length > postChange.diapers.length){
        await this.prisma.diaperChange.deleteMany({
          where: {
            change_id: change.id,
            user_id: change.user_id,
            order: {
              gt: postChange.diapers.length
            }
          }
        });
      }

    }
  }


}
