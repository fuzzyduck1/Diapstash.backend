import {Controller, Inject} from "@tsed/di";
import { PrismaService } from "@tsed/prisma";
import {ContentType, Get} from "@tsed/schema";


@Controller("/metrics")
export class MetricsController {

  @Inject()
  prisma: PrismaService;

  @Get("/prisma")
  @ContentType('text')
  async get() {
    return await this.prisma.$metrics.prometheus();
  }

  @Get("/prisma/json")
  @ContentType('json')
  async json() {
    return await this.prisma.$metrics.json();
  }
}
