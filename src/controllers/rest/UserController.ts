import {Controller, Inject} from "@tsed/di";
import { UseAuth } from "@tsed/platform-middlewares";
import { BodyParams, HeaderParams } from "@tsed/platform-params";
import {ContentType, Delete, Post} from "@tsed/schema";
import { AppUserAuthMiddleware } from "../../middlewares/AppUserAuthMiddleware";
import { PrismaService } from "@tsed/prisma";

@Controller("/user")
export class UserController {

  @Inject()
  prisma: PrismaService;

  @Post("/init")
  @ContentType("application/json")
  async init() {
    return await this.prisma.user.create({data:{}});
  }

  @Post("/join")
  @ContentType("application/json")
  async join(@BodyParams("secureToken") secureToken: string, @BodyParams("userId") userId: string) {
    let user = await this.prisma.user.findFirst({
      where: {
        id: userId,
        secureToken: secureToken
      }
    });
    if(user != null){
      return {join: true, user: user};
    }else{
      return {join: false}
    }
  }

  @Delete("/delete")
  @ContentType("application/json")
  @UseAuth(AppUserAuthMiddleware)
  async delete(@HeaderParams("x-user-id") userId: string){

    await this.prisma.user.delete({
      where: {id: userId}
    })

  }

}
