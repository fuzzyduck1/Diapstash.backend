import { SharedHistoryAccess, SharedHistoryState } from "@prisma/client";
import {Controller, Inject} from "@tsed/di";
import { NotFound } from "@tsed/exceptions";
import { UseAuth } from "@tsed/platform-middlewares";
import { BodyParams, HeaderParams } from "@tsed/platform-params";
import {Get, Post, Put} from "@tsed/schema";
import { randomUUID } from "crypto";

import { AppUserAuthMiddleware } from "../../middlewares/AppUserAuthMiddleware";
import { PrismaService } from "@tsed/prisma";

@Controller("/shared-history")
export class SharedHistoryController {

  @Inject()
  prisma: PrismaService;


  @Put("/generate")
  @UseAuth(AppUserAuthMiddleware)
  async generateSharing(@BodyParams("toName") name: string, @HeaderParams("x-user-id") userId: string){
    let securityToken = randomUUID().replace(/-/g,"");
    let sh = await this.prisma.sharedHistory.create({
      data: {
        toName: name,
        userId: userId,
        securityToken: securityToken,
        state: SharedHistoryState.PENDING
      }
    });

    return sh;
  }

  @Post("/revoke")
  async revoke(@BodyParams("id") id: number, @BodyParams("securityToken") securityToken: string){
    await this.prisma.sharedHistory.deleteMany({
      where: {
        id: id,
        securityToken: securityToken
      }
    });

    return {ok: true};
  }


  @Get("/list/mines")
  @UseAuth(AppUserAuthMiddleware)
  async getSharedHistories(@HeaderParams("x-user-id") userId: string){
    return await this.prisma.sharedHistory.findMany({
      where: {userId: userId},
      select: {id: true, toName: true, state: true, securityToken: true, fromName:false, user: false, sharedHistoriesAccess: false, userId: false}
    });
  }

  @Get("/list/access")
  @UseAuth(AppUserAuthMiddleware)
  async getSharedAccess(@HeaderParams("x-user-id") userId: string){
    return await this.prisma.sharedHistoryAccess.findMany({
      where: {userId: userId},
      select: {
        user: false, userId: false,
        sharedHistory: {
          select: {
            id: true,
            securityToken: true,
            toName: false,
            fromName: true,
            user: false,
            userId: false,
            state: true
          }
        }
      }
    });
  }

  @Post("/link/access")
  async linkAccess(@BodyParams("id") id : number, @BodyParams("securityToken") securityToken: string, @BodyParams("fromName") fromName: string, @HeaderParams("x-user-id") userId?: string){
    let sh = await this.prisma.sharedHistory.findFirst({
      where: {id: id, securityToken: securityToken}
    });
    if(sh == null){
      throw new NotFound("");
    }

    if(sh.state != SharedHistoryState.PENDING){
      return {ok: false, err_code: 'already_linked'}
    }

    sh.fromName = fromName;
    sh.state = SharedHistoryState.ACCEPTED;

    await this.prisma.sharedHistory.update({data: sh, where:{id: sh.id}});

    var sha : SharedHistoryAccess | null = null;

    if(userId != null){
      sha = await this.prisma.sharedHistoryAccess.create({
        data: {
          sharedHistoryId: sh.id,
          userId: userId
        }
      })
    }

    return {ok: sh != null, sha: sha, sh_id: sh.id};
  }

  @Post("/validate/access")
  async validateAccess(@BodyParams("id") id : number, @BodyParams("securityToken") securityToken: string){
    let sh = await this.prisma.sharedHistory.findFirst({
      where: {id: id, securityToken: securityToken}
    });
    return {ok: sh != null};
  }

  @Post("/history/lookup")
  async getHistory(@BodyParams("id") id : number, @BodyParams("securityToken") securityToken: string){
    let history = await this.prisma.sharedHistory.findFirst({
      where: {id: id, securityToken: securityToken},
      select: {
        fromName: true,
        user: {
          select: {
            id: false,
            name: false,
            secureToken: false,
            changes: {
              select: {
                diapers: {
                  select: {
                    change: false,
                    change_id: false,
                    user_id: false,
                    type: false,
                    type_id: true,
                    count: true,
                    size: true
                  }
                },
                user_id: false,
                user: false,
                id: true,
                endTime: true,
                startTime: true,
                state: true,
                wetness: true,
                note: true,
                tags: true
              }
            },
            stocks: false,
            proposals: false,
            sharedHistories: false,
            customTypes: false,
            sharedHistoriesAccess: false
          }
        }
      }
    });
    if(history == null){
      throw new NotFound("");
    }
    return history;
  }

}
