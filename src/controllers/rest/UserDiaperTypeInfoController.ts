import { Controller, Inject } from "@tsed/di";
import { Delete, Get, Post } from "@tsed/schema";

import { AppUserAuthMiddleware } from "../../middlewares/AppUserAuthMiddleware";
import { UserDiaperTypeInfo } from "@prisma/client";
import { UseAuth } from "@tsed/platform-middlewares";
import { BodyParams, HeaderParams, PathParams } from "@tsed/platform-params";
import { PrismaService } from "@tsed/prisma";

@Controller("/user-diaper-type-info")
export class UserDiaperTypeInfoController {

  @Inject()
  prisma: PrismaService;

  @Get("/:typeId")
  @UseAuth(AppUserAuthMiddleware)
  async getOne(@HeaderParams("x-user-id") userId: string, @PathParams("typeId") typeId: number) {
    const info = await this.prisma.userDiaperTypeInfo.findFirst({
      where: {
        userId: userId,
        typeId: typeId
      }
    });
    return info;
  }

  @Get("/")
  @UseAuth(AppUserAuthMiddleware)
  async getAll(@HeaderParams("x-user-id") userId: string) {
    const info = await this.prisma.userDiaperTypeInfo.findMany({
      where: {
        userId: userId
      }
    });
    return info;
  }

  @Post("/")
  @UseAuth(AppUserAuthMiddleware)
  async add(@HeaderParams("x-user-id") userId: string, @BodyParams() info: UserDiaperTypeInfo) {
    return await this.prisma.userDiaperTypeInfo.upsert({
      create: {
        userId: userId,
        typeId: info.typeId,
        thresholdLowStock: info.thresholdLowStock,
        note: info.note
      },
      update: {
        thresholdLowStock: info.thresholdLowStock,
        note: info.note
      },
      where: {
        userId_typeId: {
          userId: userId,
          typeId: info.typeId
        }
      }
    });
  }

  @Delete("/")
  @UseAuth(AppUserAuthMiddleware)
  async delete(@HeaderParams("x-user-id") userId: string, @BodyParams() info: UserDiaperTypeInfo) {
    return await this.prisma.userDiaperTypeInfo.delete({
      where: {
        userId_typeId: {
          userId: userId,
          typeId: info.typeId
        }
      }
    });
  }

  @Post("/many")
  @UseAuth(AppUserAuthMiddleware)
  async addMany(@HeaderParams("x-user-id") userId: string, @BodyParams() infos: UserDiaperTypeInfo[]) {
    for(var info of infos){
      return await this.prisma.userDiaperTypeInfo.upsert({
        create: {
          userId: userId,
          typeId: info.typeId,
          thresholdLowStock: info.thresholdLowStock,
          note: info.note
        },
        update: {
          thresholdLowStock: info.thresholdLowStock,
          note: info.note
        },
        where: {
          userId_typeId: {
            userId: userId,
            typeId: info.typeId
          }
        }
      });
    }
  }

}
