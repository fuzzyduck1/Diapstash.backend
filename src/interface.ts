import { User } from "@prisma/client";

export class AuthUser  {
    username: string;
    token?: string;
    refreshToken?: string;
    roles: string[];
}

interface AppRequest extends Request {
    user?: AuthUser | User;
  }

export type UserRequest = AppRequest ;