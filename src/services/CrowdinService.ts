import { Inject, Injectable, OnInit, Service } from "@tsed/di";
import * as crowdin from "@crowdin/crowdin-api-client";
import { CROWDIN_TOKEN, CROWDIN_PROJECT_ID } from "../config/envs";
import { PrismaService } from "@tsed/prisma";


const languages: { [key: string]: string } = { 'en': 'en', 'fr': 'fr', 'es': 'es-ES', 'nl': 'nl', 'de': 'de', 'ko': 'ko', 'it': 'it' };

@Injectable()
@Service()
export class CrowdinService implements OnInit {

    setup = false;
    fileId: number;
    strings: crowdin.SourceStringsModel.String[];
    stringTranslationsApi: crowdin.StringTranslations;
    sourceStringsApi: crowdin.SourceStrings
    projectId: number;

    @Inject()
    prisma: PrismaService;


    async $onInit() {
        if (CROWDIN_TOKEN == null || CROWDIN_PROJECT_ID == null) {
            return;
        }
        this.projectId = parseInt(CROWDIN_PROJECT_ID);
        let client = new crowdin.default({ token: CROWDIN_TOKEN });
        this.stringTranslationsApi = client.stringTranslationsApi;
        this.sourceStringsApi = client.sourceStringsApi;

        let files = await client.sourceFilesApi.listProjectFiles(this.projectId);
        let file = files.data.find(f => f.data.name == 'whatsnews.json');
        if (file != null) {
            this.fileId = file!.data.id;
        }
        this.setup = true;

        //await this.runCheck();
        setInterval(this.runCheck, 1000 * 60 * 60 * 6);

    }

    async runCheck(): Promise<boolean> {
        if (!this.setup) {
            return false;
        }
        console.log('Update whats news translations');

        const logs = await this.prisma.whatsNews.findMany({ include: { messages: true } });
        await this.loadStrings();

        for (let l of logs) {
            const base_key = `whats_news_${l.id}`;

            let msg = await this.getMessages(base_key, l.title, l.description);

            for (let lang of Object.keys(msg)) {
                await this.prisma.whatsNewsMessage.upsert({
                    where: {
                        whatsNewsId_lang: {
                            lang: lang,
                            whatsNewsId: l.id,
                        }
                    },
                    create: {
                        lang: lang,
                        whatsNewsId: l.id,
                        title: msg[lang].title,
                        description: msg[lang].description
                    },
                    update: {
                        title: msg[lang].title,
                        description: msg[lang].description
                    }
                })
            }
        }
        return true;
    }

    async _internalLoadString(offset = 0, limit = 25){
        let res = await this.sourceStringsApi.listProjectStrings(this.projectId, {
            fileId: this.fileId,
            offset: offset,
            limit: limit
        });

        let strings = res.data.map(e => e.data);
        if(res.data.length == res.pagination.limit){
            strings.push(... await this._internalLoadString(res.pagination.offset + limit))
        }

        return strings;
    }

    async loadStrings() {
        let strings = await this._internalLoadString();

        this.strings = strings;
    }

    async getFromKey(key: string, defaultVal: string): Promise<{ [key: string]: string }> {

        let messages: { [key: string]: string } = {};

        let str = this.strings.find(s => s.identifier == key);
        if (str == null) {
            console.log(this.strings.map(s => s.identifier));
            console.log(`CROWDIN : add string ${key}`);
            await this.sourceStringsApi.addString(this.projectId, {
                fileId: this.fileId,
                identifier: key,
                text: defaultVal
            });
        } else {
            for (let lang of Object.keys(languages)) {
                try {
                    let approved = await this.stringTranslationsApi.listTranslationApprovals(this.projectId, { stringId: str.id, languageId: languages[lang] });
                    if (approved.data.length > 0) {
                        let transId = approved.data[0].data.translationId;
                        let trans = await this.stringTranslationsApi.listStringTranslations(this.projectId, str.id, languages[lang]);
                        var tran = trans.data.find(t => t.data.id == transId);
                        if (tran != null) {
                            messages[lang] = tran.data.text;
                        }
                    }
                } catch (e) { }
            }
        }
        return messages;
    }


    async getMessages(key_base: string, title: string, description: string) {
        if (!this.setup) {
            return {};
        }

        let messages: { [key: string]: { [key: string]: string } } = {};

        let key_title = `${key_base}_title`;
        let key_desc = `${key_base}_description`;

        let dataTitle = await this.getFromKey(key_title, title);
        let dataDesc = await this.getFromKey(key_desc, description);

        for (let lang in dataTitle) {
            messages[lang] = { 'title': dataTitle[lang] };
        }

        for (let lang in dataDesc) {
            messages[lang] = {
                ...messages[lang],
                'description': dataDesc[lang]
            };
        }

        return messages
    }

}
