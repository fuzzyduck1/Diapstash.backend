import dotenv from "dotenv"

export const envs = {
  ...process.env,
  ...dotenv.config().parsed
};
export const isProduction = envs.NODE_ENV === "production";
export const S3_HOST = envs.S3_HOST;
export const S3_BUCKET = envs.S3_BUCKET;
export const S3_ACCESS_KEY = envs.S3_ACCESS_KEY;
export const S3_SECRET_KEY = envs.S3_SECRET_KEY;
export const IMAGE_URL = envs.IMAGE_URL;
export const ADMIN_TOKEN = envs.ADMIN_TOKEN || "admin_token";
export const JWT_SECRET = envs.JWT_SECRET || "secret_token";
export const REFRESH_SECRET = envs.REFRESH_SECRET || "refresh_token";
export const CROWDIN_TOKEN = envs.CROWDIN_TOKEN;
export const CROWDIN_PROJECT_ID = envs.CROWDIN_PROJECT_ID;
export const NO_CACHE_DOMAIN = envs.NO_CACHE_DOMAIN!;
export const REMOVE_BG_API = envs.REMOVE_BG_API!;
export const SENTRY_DSN = envs.SENTRY_DSN;
