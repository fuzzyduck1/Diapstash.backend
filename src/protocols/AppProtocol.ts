import { Req } from "@tsed/common";
import { Unauthorized } from "@tsed/exceptions";
import { Protocol, OnVerify, OnInstall } from "@tsed/passport";
import { Inject } from "@tsed/di";
import { Strategy, VerifyCallback } from "passport-custom";
import { Request } from "express";
import { PrismaService } from "@tsed/prisma";


interface CustomStrategyOptions {
    optional: boolean
}

class CustomStrategy extends Strategy {

    constructor(options: any, verify: VerifyCallback) {
        super(verify);
    }

    authenticate(req: Request, options?: CustomStrategyOptions) {
        const headers = req.headers;

        const userId = headers["x-user-id"] as string;
        const secureToken = headers["x-secure-token"] as string;

        if(userId == null || secureToken == null){
            if(options?.optional === true){
                this.pass();
            }else{
                this.fail('', 401);
            }
            return;
        }
        super.authenticate(req, options);
    }

}

@Protocol({
    name: "app",
    useStrategy: CustomStrategy,
    settings: {
        optional: false
    }
})
export class AppProtocol implements OnVerify, OnInstall {
    @Inject()
    prisma: PrismaService;

    async $onVerify(@Req() request: Req) {
        const headers = request.headers;

        const userId = headers["x-user-id"] as string;
        const secureToken = headers["x-secure-token"] as string;

        if(userId == null || secureToken == null){
            return;
        }

        const user = await this.prisma.user.findFirst({
            where: {
                id: userId,
                secureToken: secureToken
            }
        });

        if (user == null) {
            throw new Unauthorized("Authentification failed");
        }

        request.user = user;

        return user;
    }

    $onInstall(strategy: CustomStrategy): void {
        // intercept the strategy instance to adding extra configuration
    }
}

