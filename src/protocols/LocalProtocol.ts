import { BodyParams, Req } from "@tsed/common";
import { Strategy } from "passport-local";
import { Unauthorized } from "@tsed/exceptions";
import { Protocol, OnInstall, OnVerify } from "@tsed/passport";
import { Inject } from "@tsed/di";
import { AdminAccount } from "@prisma/client";
import * as bcrypt from "bcrypt";
import { Required } from "@tsed/schema";
import jwt from "jsonwebtoken";
import { JWT_SECRET, REFRESH_SECRET } from "../config/envs";
import { AuthUser } from "../interface";
import { PrismaService } from "@tsed/prisma";

export class Credentials {
    @Required()
    username: string;

    @Required()
    password: string;
}

@Protocol({
    name: "local",
    useStrategy: Strategy,
    settings: {
        usernameField: "username",
        passwordField: "password"
    }
})
export class LocalProtocol implements OnVerify, OnInstall {
    @Inject()
    prisma: PrismaService;

    async $onVerify(@Req() request: Req, @BodyParams() credentials: Credentials) {
        let user = await this.prisma.adminAccount.findFirst({
            where: {
                username: credentials.username
            }
        });

        if (user == null) {
            throw new Unauthorized('No user found or password invalid');
        }

        if (!await bcrypt.compare(credentials.password, user.password)) {
            throw new Unauthorized('No user found or password invalid.');
        }


        let token = buildToken(user);
        let refreshToken = buildRefreshToken(user);

        let authUser: AuthUser = { username: user.username, token: token, roles: user.roles, refreshToken: refreshToken };

        request.user = authUser;

        return authUser;
    }

    $onInstall(strategy: Strategy): void {
        // intercept the strategy instance to adding extra configuration
    }
}


export function buildToken(user: AdminAccount) {
    let token = jwt.sign({
        username: user.username,
        roles: user.roles,
        requireChangePassword: user.requireChangePassword
    }, JWT_SECRET, {
        expiresIn: "30m"
    });

    return token;
}

export function buildRefreshToken(user: AdminAccount) {
    let token = jwt.sign({
        username: user.username,
        roles: user.roles,
        requireChangePassword: user.requireChangePassword
    }, REFRESH_SECRET, {
        expiresIn: "7d"
    });

    return token;
}