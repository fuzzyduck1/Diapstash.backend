import { Req } from "@tsed/common";
import { ExtractJwt, Strategy } from "passport-jwt";
import { Unauthorized } from "@tsed/exceptions";
import { Protocol, OnInstall, OnVerify, Arg } from "@tsed/passport";
import { Inject } from "@tsed/di";
import { JWT_SECRET } from "../config/envs";
import { AuthUser } from "../interface";
import { PrismaService } from "@tsed/prisma";


@Protocol({
    name: "jwt",
    useStrategy: Strategy,
    settings: {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: JWT_SECRET,
    }
})
export class JWTProtocol implements OnVerify, OnInstall {
    @Inject()
    prisma: PrismaService;

    async $onVerify(@Req() request: Req, @Arg(0) jwtPayload: any) {
        let user = await this.prisma.adminAccount.findFirst({
            where: {
                username: jwtPayload.username
            }
        });

        if (user == null) {
            throw new Unauthorized('No user found or password invalid');
        }

        let authUser : AuthUser = {
            username: user.username,
            roles: user.roles,
        };
        request.user = authUser;
        return authUser;
    }

    $onInstall(strategy: Strategy): void {
    }
}