import { Inject, Req } from "@tsed/common";
import { Unauthorized } from "@tsed/exceptions";
import { Middleware, MiddlewareMethods } from "@tsed/platform-middlewares";
import { Context } from "@tsed/platform-params";
import { JWT_SECRET } from "../config/envs";
import jwt from "jsonwebtoken";
import * as Sentry from "@sentry/node";
import { PrismaService } from "@tsed/prisma";

@Middleware()
export class AdminAuthMiddleware implements MiddlewareMethods {

  @Inject()
  prisma: PrismaService;

  async use(@Req() request: Req, @Context() ctx: Context) {

    const headers = request.headers;
    if (headers["x-auth"] == null) {
      throw new Unauthorized("No x-auth");
    }

    const auth = headers["x-auth"] as string;

    let verified = jwt.verify(auth, JWT_SECRET);
    if(verified ){
      let username = (verified as jwt.JwtPayload).username;
      let user = await this.prisma.adminAccount.findFirst({
        where: {
          username: username
        }
      });

      if (user == null) {
        throw new Unauthorized('No user found or password invalid');
      }
      Sentry.configureScope(function(scope) {
        scope.setUser({
          name: user!.username,
          admin: true
        });
      });
      ctx.set("user", user);

      return user;
    }else{
      throw new Unauthorized("Auth non valid");
    }
  }
}
