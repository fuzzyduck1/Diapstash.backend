import { Req } from "@tsed/common";
import { Middleware, MiddlewareMethods } from "@tsed/platform-middlewares";
import { Context } from "@tsed/platform-params";
import promClient from "prom-client";


const versionRequestCounter = new promClient.Counter({
  name: "ds_http_total_version",
  help: 'http request with version for ds',
  labelNames: ['version_name', 'version_code', 'method', 'path']
});


@Middleware()
export class PrometheusMiddleware implements MiddlewareMethods {


  use(@Context() ctx: Context, @Req() req: Req) {


    if(req.path === '/rest/health' || (req.path as string).startsWith('/rest/metrics') || (req.path as string).startsWith('/metrics')) return;

    let versionName = req.header('X-DS-VERSION-NAME') ?? ''
    let versionCode = req.header('X-DS-VERSION-CODE') ?? '';

    versionRequestCounter.inc({
      version_code: versionCode, version_name: versionName,
      method: req.method, path: req.path
    }, 1);

  }
}
