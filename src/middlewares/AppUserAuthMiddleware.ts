import { Inject, Req } from "@tsed/common";
import { Unauthorized } from "@tsed/exceptions";
import {Middleware, MiddlewareMethods} from "@tsed/platform-middlewares";
import {Context} from "@tsed/platform-params";
import * as Sentry from "@sentry/node";
import { PrismaService } from "@tsed/prisma";

@Middleware()
export class AppUserAuthMiddleware implements MiddlewareMethods {

  @Inject()
  prisma: PrismaService;

  async use(@Req() request: Req, @Context() ctx: Context) {

    const user = await getUserFromReq(this.prisma, request);

    if(user == null){
      throw new Unauthorized("Authentification failed");
    }

    Sentry.configureScope(function(scope) {
      scope.setUser({
        id: user.id,
        admin: false
      });
    });
    ctx.set("user", user);
    request.user = user;
    return user;
  }
}


export async function getUserFromReq(prisma: PrismaService, request: Req){
  const headers = request.headers;
  if(headers["x-user-id"] == null || headers["x-secure-token"] == null){
    throw new Unauthorized("No x-user-id and x-secure-token headers");
  }

  const userId = headers["x-user-id"] as string;
  const secureToken = headers["x-secure-token"] as string;

  const user = await prisma.user.findFirst({
    where: {
      id: userId,
      secureToken: secureToken
    }
  });
  return user;
}
