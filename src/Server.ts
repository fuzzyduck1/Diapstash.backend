import { join } from "path";
import { Configuration, Inject } from "@tsed/di";
import { PlatformApplication } from "@tsed/common";
import "@tsed/platform-express"; // /!\ keep this import
import "@tsed/ajv";
import { config } from "./config/index";
import * as rest from "./controllers/rest/index";
import * as Sentry from "@sentry/node";
import { AuthUser } from "./interface";
import session from 'express-session';
import { ExpressPrometheusMiddleware } from '@matteodisabatino/express-prometheus-middleware';
import "./protocols";
import { PrometheusMiddleware } from "./middlewares/PrometheusMiddleware";
import { PrismaService } from "@tsed/prisma";

@Configuration({
  ...config,
  acceptMimes: ["application/json"],
  httpPort: process.env.PORT || 8083,
  httpsPort: false, // CHANGE
  componentsScan: true,
  mount: {
    "/rest": [
      ...Object.values(rest)
    ]
  },
  middlewares: [
    { use: "cors", options: { origin: '*' } },
    "cookie-parser",
    "compression",
    "method-override",
    "json-parser",
    { use: "urlencoded-parser", options: { extended: true } },
    session({
      secret: "secret",
      resave: true,
      saveUninitialized: true
    })
  ],
  views: {
    root: join(process.cwd(), "../views"),
    extensions: {
      ejs: "ejs"
    }
  },
  exclude: [
    "**/*.spec.ts"
  ],
  passport: {
    disableSession: true,
    userInfoModel: AuthUser
  },
  cache: {
    ttl: null,
    max: 100,
    store: "memory",
  }
})
export class Server {
  @Inject()
  protected app: PlatformApplication;

  @Configuration()
  protected settings: Configuration;

  @Inject()
  prisma: PrismaService;


  $beforeRoutesInit() {

    this.app.use(Sentry.Handlers.requestHandler());
    this.app.use(Sentry.Handlers.tracingHandler());

    //this.app.use((req: any,res: any,next: any) => {setTimeout(next,1_000)});

    const epm = new ExpressPrometheusMiddleware({
      exclude: (req) => req.path === '/rest/health' || (req.path as string).startsWith('/rest/metrics') || (req.path as string).startsWith('/metrics'),
    });

    this.app.use(epm.handler);
    this.app.use(PrometheusMiddleware);

    new Sentry.Integrations.Express({ app: this.app.rawApp });
  }

  $afterRoutesInit() {
    this.app.use(Sentry.Handlers.errorHandler());
    new Sentry.Integrations.Prisma({
      client: this.prisma
    });

  }
}
