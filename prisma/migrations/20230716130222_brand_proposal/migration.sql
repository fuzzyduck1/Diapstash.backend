-- AlterTable
ALTER TABLE "ProcessProposal" ADD COLUMN     "brandProposalId" INTEGER;

-- CreateTable
CREATE TABLE "BrandProposal" (
    "id" SERIAL NOT NULL,
    "patch" JSONB NOT NULL,
    "sourceData" TEXT NOT NULL,
    "state" "ProposalState" NOT NULL,
    "originalBrandCode" TEXT,
    "proposalId" INTEGER,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3),

    CONSTRAINT "BrandProposal_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "BrandProposal" ADD CONSTRAINT "BrandProposal_originalBrandCode_fkey" FOREIGN KEY ("originalBrandCode") REFERENCES "DiaperBrand"("code") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "BrandProposal" ADD CONSTRAINT "BrandProposal_proposalId_fkey" FOREIGN KEY ("proposalId") REFERENCES "Proposal"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ProcessProposal" ADD CONSTRAINT "ProcessProposal_brandProposalId_fkey" FOREIGN KEY ("brandProposalId") REFERENCES "BrandProposal"("id") ON DELETE SET NULL ON UPDATE CASCADE;
