-- CreateTable
CREATE TABLE "ProcessProposal" (
    "id" SERIAL NOT NULL,
    "proposalId" INTEGER NOT NULL,
    "state" "ProposalState",
    "proposedTypeId" INTEGER,

    CONSTRAINT "ProcessProposal_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "ProcessProposal" ADD CONSTRAINT "ProcessProposal_proposalId_fkey" FOREIGN KEY ("proposalId") REFERENCES "Proposal"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ProcessProposal" ADD CONSTRAINT "ProcessProposal_proposedTypeId_fkey" FOREIGN KEY ("proposedTypeId") REFERENCES "DiaperType"("id") ON DELETE SET NULL ON UPDATE CASCADE;
