-- CreateEnum
CREATE TYPE "EType" AS ENUM ('DIAPER', 'BOOSTER');

-- AlterTable
ALTER TABLE "DiaperType" ADD COLUMN     "type" "EType" NOT NULL DEFAULT 'DIAPER';
