CREATE VIEW "community_rating" AS
    SELECT r.type_id, r.criteria, round(avg(r.note)::numeric(10, 2)*2, 0) / 2 as note
    FROM "rating" r
    group by r.type_id, r.criteria;
