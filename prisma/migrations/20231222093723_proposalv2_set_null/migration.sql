-- DropForeignKey
ALTER TABLE "brand_process_proposal" DROP CONSTRAINT "brand_process_proposal_original_brand_fkey";

-- DropForeignKey
ALTER TABLE "brand_proposal_v2" DROP CONSTRAINT "brand_proposal_v2_original_brand_fkey";

-- DropForeignKey
ALTER TABLE "brand_proposal_v2" DROP CONSTRAINT "brand_proposal_v2_result_brand_fkey";

-- DropForeignKey
ALTER TABLE "type_process_proposal" DROP CONSTRAINT "type_process_proposal_original_type_fkey";

-- DropForeignKey
ALTER TABLE "type_proposal_v2" DROP CONSTRAINT "type_proposal_v2_original_type_fkey";

-- DropForeignKey
ALTER TABLE "type_proposal_v2" DROP CONSTRAINT "type_proposal_v2_result_type_fkey";

-- AddForeignKey
ALTER TABLE "type_proposal_v2" ADD CONSTRAINT "type_proposal_v2_original_type_fkey" FOREIGN KEY ("original_type") REFERENCES "DiaperType"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "type_proposal_v2" ADD CONSTRAINT "type_proposal_v2_result_type_fkey" FOREIGN KEY ("result_type") REFERENCES "DiaperType"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "type_process_proposal" ADD CONSTRAINT "type_process_proposal_original_type_fkey" FOREIGN KEY ("original_type") REFERENCES "DiaperType"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "brand_proposal_v2" ADD CONSTRAINT "brand_proposal_v2_original_brand_fkey" FOREIGN KEY ("original_brand") REFERENCES "DiaperBrand"("code") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "brand_proposal_v2" ADD CONSTRAINT "brand_proposal_v2_result_brand_fkey" FOREIGN KEY ("result_brand") REFERENCES "DiaperBrand"("code") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "brand_process_proposal" ADD CONSTRAINT "brand_process_proposal_original_brand_fkey" FOREIGN KEY ("original_brand") REFERENCES "DiaperBrand"("code") ON DELETE SET NULL ON UPDATE CASCADE;
