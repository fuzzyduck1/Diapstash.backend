-- DropForeignKey
ALTER TABLE "DiaperChange" DROP CONSTRAINT "DiaperChange_change_id_user_id_fkey";

-- AddForeignKey
ALTER TABLE "DiaperChange" ADD CONSTRAINT "DiaperChange_change_id_user_id_fkey" FOREIGN KEY ("change_id", "user_id") REFERENCES "Change"("id", "user_id") ON DELETE CASCADE ON UPDATE CASCADE;
