-- AlterTable
ALTER TABLE "TypeProposal" ADD COLUMN     "originTypeProposalId" INTEGER;

-- AddForeignKey
ALTER TABLE "TypeProposal" ADD CONSTRAINT "TypeProposal_originTypeProposalId_fkey" FOREIGN KEY ("originTypeProposalId") REFERENCES "TypeProposal"("id") ON DELETE SET NULL ON UPDATE CASCADE;
