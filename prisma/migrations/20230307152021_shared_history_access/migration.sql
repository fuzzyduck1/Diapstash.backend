-- CreateTable
CREATE TABLE "SharedHistoryAccess" (
    "userId" TEXT NOT NULL,
    "sharedHistoryId" INTEGER NOT NULL,

    CONSTRAINT "SharedHistoryAccess_pkey" PRIMARY KEY ("userId","sharedHistoryId")
);

-- AddForeignKey
ALTER TABLE "SharedHistoryAccess" ADD CONSTRAINT "SharedHistoryAccess_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SharedHistoryAccess" ADD CONSTRAINT "SharedHistoryAccess_sharedHistoryId_fkey" FOREIGN KEY ("sharedHistoryId") REFERENCES "SharedHistory"("id") ON DELETE CASCADE ON UPDATE CASCADE;
