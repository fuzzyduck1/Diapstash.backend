/*
  Warnings:

  - Added the required column `username` to the `ProcessProposal` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "ProcessProposal" ADD COLUMN     "username" TEXT NOT NULL;

-- AddForeignKey
ALTER TABLE "ProcessProposal" ADD CONSTRAINT "ProcessProposal_username_fkey" FOREIGN KEY ("username") REFERENCES "AdminAccount"("username") ON DELETE CASCADE ON UPDATE CASCADE;
