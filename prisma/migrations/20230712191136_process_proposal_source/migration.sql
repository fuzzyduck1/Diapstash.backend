/*
  Warnings:

  - Added the required column `source` to the `ProcessProposal` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "ProcessProposal" ADD COLUMN     "source" TEXT NOT NULL;
