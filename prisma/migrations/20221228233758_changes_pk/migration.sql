/*
  Warnings:

  - The primary key for the `DiaperChange` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `id` on the `DiaperChange` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "DiaperChange" DROP CONSTRAINT "DiaperChange_pkey",
DROP COLUMN "id",
ADD COLUMN     "count" INTEGER NOT NULL DEFAULT 1,
ADD CONSTRAINT "DiaperChange_pkey" PRIMARY KEY ("change_id", "user_id", "type_id");
