-- DropForeignKey
ALTER TABLE "DiaperStock" DROP CONSTRAINT "DiaperStock_userId_fkey";

-- AddForeignKey
ALTER TABLE "DiaperStock" ADD CONSTRAINT "DiaperStock_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
