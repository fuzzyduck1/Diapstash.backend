/*
  Warnings:

  - You are about to drop the column `count` on the `DiaperStock` table. All the data in the column will be lost.
  - Added the required column `counts` to the `DiaperStock` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "DiaperStock" DROP COLUMN "count",
ADD COLUMN     "counts" JSONB NOT NULL;
