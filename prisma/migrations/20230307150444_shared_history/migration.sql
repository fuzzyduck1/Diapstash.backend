-- CreateEnum
CREATE TYPE "SharedHistoryState" AS ENUM ('PENDING', 'ACCEPTED');

-- CreateTable
CREATE TABLE "SharedHistory" (
    "id" SERIAL NOT NULL,
    "userId" TEXT NOT NULL,
    "securityToken" TEXT NOT NULL,
    "fromName" TEXT,
    "toName" TEXT NOT NULL,
    "state" "SharedHistoryState" NOT NULL,

    CONSTRAINT "SharedHistory_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "SharedHistory" ADD CONSTRAINT "SharedHistory_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
