/*
  Warnings:

  - You are about to drop the `rating_user` table. If the table is not empty, all the data it contains will be lost.

*/
create or replace view community_rating(type_id, criteria, note) as
SELECT r.type_id,
       r.criteria,
       round(avg(r.note)::numeric(10, 2) * 2::numeric, 0) / 2::numeric AS note,
       min(r.note)::numeric AS min_note,
       max(r.note)::numeric AS max_note,
       count(r.note)::numeric AS nb_note
FROM rating r
GROUP BY r.type_id, r.criteria;