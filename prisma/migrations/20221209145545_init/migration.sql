-- CreateTable
CREATE TABLE "User" (
    "id" TEXT NOT NULL,
    "name" TEXT,
    "secureToken" TEXT NOT NULL,

    CONSTRAINT "User_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "DiaperType" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "brand" TEXT NOT NULL,
    "image" TEXT,
    "availableSizes" TEXT[] DEFAULT ARRAY['M', 'L']::TEXT[],

    CONSTRAINT "DiaperType_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "DiaperStock" (
    "diaperType_id" INTEGER NOT NULL,
    "userId" TEXT NOT NULL,

    CONSTRAINT "DiaperStock_pkey" PRIMARY KEY ("diaperType_id","userId")
);

-- CreateTable
CREATE TABLE "DiaperChange" (
    "id" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    "startTime" TIMESTAMP(3) NOT NULL,
    "endTime" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "DiaperChange_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_DiaperChangeToDiaperType" (
    "A" TEXT NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_DiaperChangeToDiaperType_AB_unique" ON "_DiaperChangeToDiaperType"("A", "B");

-- CreateIndex
CREATE INDEX "_DiaperChangeToDiaperType_B_index" ON "_DiaperChangeToDiaperType"("B");

-- AddForeignKey
ALTER TABLE "DiaperStock" ADD CONSTRAINT "DiaperStock_diaperType_id_fkey" FOREIGN KEY ("diaperType_id") REFERENCES "DiaperType"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "DiaperStock" ADD CONSTRAINT "DiaperStock_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "DiaperChange" ADD CONSTRAINT "DiaperChange_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_DiaperChangeToDiaperType" ADD CONSTRAINT "_DiaperChangeToDiaperType_A_fkey" FOREIGN KEY ("A") REFERENCES "DiaperChange"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_DiaperChangeToDiaperType" ADD CONSTRAINT "_DiaperChangeToDiaperType_B_fkey" FOREIGN KEY ("B") REFERENCES "DiaperType"("id") ON DELETE CASCADE ON UPDATE CASCADE;
