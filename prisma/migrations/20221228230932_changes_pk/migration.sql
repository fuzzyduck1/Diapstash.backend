/*
  Warnings:

  - The primary key for the `Change` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - Added the required column `user_id` to the `DiaperChange` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "DiaperChange" DROP CONSTRAINT "DiaperChange_change_id_fkey";

-- AlterTable
ALTER TABLE "Change" DROP CONSTRAINT "Change_pkey",
ADD CONSTRAINT "Change_pkey" PRIMARY KEY ("id", "userId");

-- AlterTable
ALTER TABLE "DiaperChange" ADD COLUMN     "user_id" TEXT NOT NULL;

-- AddForeignKey
ALTER TABLE "DiaperChange" ADD CONSTRAINT "DiaperChange_change_id_user_id_fkey" FOREIGN KEY ("change_id", "user_id") REFERENCES "Change"("id", "userId") ON DELETE RESTRICT ON UPDATE CASCADE;
