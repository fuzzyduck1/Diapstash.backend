-- AlterTable
ALTER TABLE "AdminAccount" ADD COLUMN     "isAdmin" BOOLEAN NOT NULL DEFAULT false;

-- CreateTable
CREATE TABLE "TypeProposal" (
    "id" SERIAL NOT NULL,
    "patch" JSONB NOT NULL,
    "sourceData" TEXT NOT NULL,
    "state" "ProposalState" NOT NULL,
    "originalTypeId" INTEGER,
    "proposalId" INTEGER,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3),

    CONSTRAINT "TypeProposal_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "TypeProposal" ADD CONSTRAINT "TypeProposal_originalTypeId_fkey" FOREIGN KEY ("originalTypeId") REFERENCES "DiaperType"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TypeProposal" ADD CONSTRAINT "TypeProposal_proposalId_fkey" FOREIGN KEY ("proposalId") REFERENCES "Proposal"("id") ON DELETE SET NULL ON UPDATE CASCADE;
