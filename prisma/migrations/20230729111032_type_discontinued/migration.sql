-- AlterTable
ALTER TABLE "DiaperType" ADD COLUMN     "discontinued" BOOLEAN NOT NULL DEFAULT false;

-- AlterTable
ALTER TABLE "Proposal" ADD COLUMN     "discontinued" BOOLEAN NOT NULL DEFAULT false,
ALTER COLUMN "availableSizes" SET DEFAULT ARRAY[]::TEXT[];
