/*
  Warnings:

  - The primary key for the `Change` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `userId` on the `Change` table. All the data in the column will be lost.
  - Added the required column `user_id` to the `Change` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "Change" DROP CONSTRAINT "Change_userId_fkey";

-- DropForeignKey
ALTER TABLE "DiaperChange" DROP CONSTRAINT "DiaperChange_change_id_user_id_fkey";

-- AlterTable
ALTER TABLE "Change" DROP CONSTRAINT "Change_pkey",
DROP COLUMN "userId",
ADD COLUMN     "user_id" TEXT NOT NULL,
ADD CONSTRAINT "Change_pkey" PRIMARY KEY ("id", "user_id");

-- AddForeignKey
ALTER TABLE "Change" ADD CONSTRAINT "Change_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "DiaperChange" ADD CONSTRAINT "DiaperChange_change_id_user_id_fkey" FOREIGN KEY ("change_id", "user_id") REFERENCES "Change"("id", "user_id") ON DELETE RESTRICT ON UPDATE CASCADE;
