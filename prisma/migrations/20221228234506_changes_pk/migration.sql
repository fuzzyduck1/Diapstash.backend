/*
  Warnings:

  - The primary key for the `Change` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `DiaperChange` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - Changed the type of `id` on the `Change` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `change_id` on the `DiaperChange` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.

*/
-- DropForeignKey
ALTER TABLE "DiaperChange" DROP CONSTRAINT "DiaperChange_change_id_user_id_fkey";

-- AlterTable
ALTER TABLE "Change" DROP CONSTRAINT "Change_pkey",
DROP COLUMN "id",
ADD COLUMN     "id" INTEGER NOT NULL,
ADD CONSTRAINT "Change_pkey" PRIMARY KEY ("id", "userId");

-- AlterTable
ALTER TABLE "DiaperChange" DROP CONSTRAINT "DiaperChange_pkey",
DROP COLUMN "change_id",
ADD COLUMN     "change_id" INTEGER NOT NULL,
ADD CONSTRAINT "DiaperChange_pkey" PRIMARY KEY ("change_id", "user_id", "type_id", "size");

-- AddForeignKey
ALTER TABLE "DiaperChange" ADD CONSTRAINT "DiaperChange_change_id_user_id_fkey" FOREIGN KEY ("change_id", "user_id") REFERENCES "Change"("id", "userId") ON DELETE RESTRICT ON UPDATE CASCADE;
