-- CreateTable
CREATE TABLE "UserDiaperTypeInfo" (
    "userId" TEXT NOT NULL,
    "typeId" INTEGER NOT NULL,
    "thresholdLowStock" INTEGER,
    "note" TEXT,

    CONSTRAINT "UserDiaperTypeInfo_pkey" PRIMARY KEY ("userId","typeId")
);

-- AddForeignKey
ALTER TABLE "UserDiaperTypeInfo" ADD CONSTRAINT "UserDiaperTypeInfo_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "UserDiaperTypeInfo" ADD CONSTRAINT "UserDiaperTypeInfo_typeId_fkey" FOREIGN KEY ("typeId") REFERENCES "DiaperType"("id") ON DELETE CASCADE ON UPDATE CASCADE;
