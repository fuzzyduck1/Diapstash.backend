-- AlterTable
ALTER TABLE "Change" ADD COLUMN     "price" DOUBLE PRECISION;

-- AlterTable
ALTER TABLE "DiaperChange" ADD COLUMN     "price" DOUBLE PRECISION;

-- AlterTable
ALTER TABLE "DiaperStock" ADD COLUMN     "prices" JSONB NOT NULL DEFAULT '{}';
