/*
  Warnings:

  - The primary key for the `DiaperStock` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `diaperType_id` on the `DiaperStock` table. All the data in the column will be lost.
  - You are about to drop the column `size` on the `DiaperStock` table. All the data in the column will be lost.
  - Added the required column `diaperTypeId` to the `DiaperStock` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "DiaperStock" DROP CONSTRAINT "DiaperStock_diaperType_id_fkey";

-- AlterTable
ALTER TABLE "DiaperStock" DROP CONSTRAINT "DiaperStock_pkey",
DROP COLUMN "diaperType_id",
DROP COLUMN "size",
ADD COLUMN     "diaperTypeId" INTEGER NOT NULL,
ADD CONSTRAINT "DiaperStock_pkey" PRIMARY KEY ("diaperTypeId", "userId");

-- AddForeignKey
ALTER TABLE "DiaperStock" ADD CONSTRAINT "DiaperStock_diaperTypeId_fkey" FOREIGN KEY ("diaperTypeId") REFERENCES "DiaperType"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
