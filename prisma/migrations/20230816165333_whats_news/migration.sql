-- CreateTable
CREATE TABLE "whats_news" (
    "id" SERIAL NOT NULL,
    "build_code" INTEGER NOT NULL,
    "release_name" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "image" TEXT,

    CONSTRAINT "whats_news_pkey" PRIMARY KEY ("id")
);
