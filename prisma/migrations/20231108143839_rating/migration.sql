/*
  Warnings:

  - You are about to drop the `rating_user` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "rating_user" DROP CONSTRAINT "rating_user_rating_id_fkey";

-- DropForeignKey
ALTER TABLE "rating_user" DROP CONSTRAINT "rating_user_user_id_fkey";

-- DropTable
DROP TABLE "rating_user";

-- CreateTable
CREATE TABLE "rating_cloud_sync" (
    "rating_id" TEXT NOT NULL,
    "user_id" TEXT NOT NULL,

    CONSTRAINT "rating_cloud_sync_pkey" PRIMARY KEY ("rating_id","user_id")
);

-- CreateIndex
CREATE UNIQUE INDEX "rating_cloud_sync_rating_id_key" ON "rating_cloud_sync"("rating_id");

-- AddForeignKey
ALTER TABLE "rating_cloud_sync" ADD CONSTRAINT "rating_cloud_sync_rating_id_fkey" FOREIGN KEY ("rating_id") REFERENCES "rating"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "rating_cloud_sync" ADD CONSTRAINT "rating_cloud_sync_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
