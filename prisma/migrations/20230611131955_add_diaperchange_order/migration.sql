-- AlterTable
ALTER TABLE "DiaperChange" ADD COLUMN     "order" INTEGER NOT NULL DEFAULT 1;

with v_table as (SELECT DC.*,
                        row_number()
                        OVER (PARTITION BY change_id, user_id ORDER BY DT.type, DT.official desc, DT.id) AS "rnum"
                        FROM "DiaperChange" DC
                        JOIN "DiaperType" DT on DT.id = DC.type_id)
UPDATE "DiaperChange" DC
set "order" = v_table."rnum"
FROM v_table
WHERE DC.change_id = v_table.change_id
  and DC.user_id = v_table.user_id
  and DC.type_id = v_table.type_id
  and DC.size = v_table.size;


ALTER TABLE "DiaperChange" DROP CONSTRAINT "DiaperChange_pkey",
ADD CONSTRAINT "DiaperChange_pkey" PRIMARY KEY ("change_id", "user_id", "type_id", "size", "order");

do
$$
declare
    f record;
    f1 int;
    i int;
begin
    for f in
        select DC.* from "DiaperChange" DC where count > 1
    loop
        select  max("order") into f1 from "DiaperChange" DC  where DC.user_id=f.user_id and DC.change_id = f.change_id limit 1;
        for i in  1..(f.count-1) loop
            insert into "DiaperChange"
            values (f.size, f.type_id, f.user_id, 1, f.change_id, f."createdAt", f."updatedAt", f1+i);
        end loop;
        f.count = 1;
        update "DiaperChange" set count=1 where size=f.size and type_id=f.type_id and user_id=f.user_id and change_id=f.change_id and "order"=f."order";

    end loop;
end;
$$;


ALTER TABLE "DiaperChange" DROP CONSTRAINT "DiaperChange_pkey",
ADD CONSTRAINT "DiaperChange_pkey" PRIMARY KEY ("change_id", "user_id", "order");