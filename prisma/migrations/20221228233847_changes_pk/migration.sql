/*
  Warnings:

  - The primary key for the `DiaperChange` table will be changed. If it partially fails, the table could be left without primary key constraint.

*/
-- AlterTable
ALTER TABLE "DiaperChange" DROP CONSTRAINT "DiaperChange_pkey",
ADD CONSTRAINT "DiaperChange_pkey" PRIMARY KEY ("change_id", "user_id", "type_id", "size");
