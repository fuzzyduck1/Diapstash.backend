/*
  Warnings:

  - You are about to drop the column `brand` on the `DiaperType` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "DiaperType" DROP COLUMN "brand",
ADD COLUMN     "brand_code" TEXT;

-- CreateTable
CREATE TABLE "DiaperBrand" (
    "code" TEXT NOT NULL,
    "name" TEXT NOT NULL,

    CONSTRAINT "DiaperBrand_pkey" PRIMARY KEY ("code")
);

-- AddForeignKey
ALTER TABLE "DiaperType" ADD CONSTRAINT "DiaperType_brand_code_fkey" FOREIGN KEY ("brand_code") REFERENCES "DiaperBrand"("code") ON DELETE SET NULL ON UPDATE CASCADE;
