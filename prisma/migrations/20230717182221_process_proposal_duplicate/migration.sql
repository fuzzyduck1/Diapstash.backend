-- AlterTable
ALTER TABLE "ProcessProposal" ADD COLUMN     "duplicateProposalId" INTEGER;

-- AddForeignKey
ALTER TABLE "ProcessProposal" ADD CONSTRAINT "ProcessProposal_duplicateProposalId_fkey" FOREIGN KEY ("duplicateProposalId") REFERENCES "Proposal"("id") ON DELETE CASCADE ON UPDATE CASCADE;
