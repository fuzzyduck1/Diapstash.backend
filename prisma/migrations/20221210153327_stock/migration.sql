/*
  Warnings:

  - The primary key for the `DiaperStock` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - Added the required column `size` to the `DiaperStock` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "DiaperStock" DROP CONSTRAINT "DiaperStock_pkey",
ADD COLUMN     "count" INTEGER NOT NULL DEFAULT 0,
ADD COLUMN     "size" TEXT NOT NULL,
ADD CONSTRAINT "DiaperStock_pkey" PRIMARY KEY ("diaperType_id", "userId", "size");
