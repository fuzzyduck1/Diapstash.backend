-- CreateEnum
CREATE TYPE "ProposalState" AS ENUM ('WAITING', 'APPROVED', 'REJECTED');

-- CreateTable
CREATE TABLE "Proposal" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "brand_code" TEXT,
    "image" TEXT,
    "availableSizes" TEXT[] DEFAULT ARRAY['M', 'L']::TEXT[],
    "state" "ProposalState" NOT NULL DEFAULT 'WAITING',
    "comment" TEXT,
    "userId" TEXT,
    "original_type_id" INTEGER,

    CONSTRAINT "Proposal_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Proposal" ADD CONSTRAINT "Proposal_brand_code_fkey" FOREIGN KEY ("brand_code") REFERENCES "DiaperBrand"("code") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Proposal" ADD CONSTRAINT "Proposal_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Proposal" ADD CONSTRAINT "Proposal_original_type_id_fkey" FOREIGN KEY ("original_type_id") REFERENCES "DiaperType"("id") ON DELETE SET NULL ON UPDATE CASCADE;
