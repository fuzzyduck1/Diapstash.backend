/*
  Warnings:

  - You are about to drop the `custom_informations_type` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "custom_informations_type" DROP CONSTRAINT "custom_informations_type_custom_information_type_fkey";

-- DropForeignKey
ALTER TABLE "custom_informations_type" DROP CONSTRAINT "custom_informations_type_type_id_fkey";

-- DropTable
DROP TABLE "custom_informations_type";

-- CreateTable
CREATE TABLE "custom_informations_data" (
    "type_id" INTEGER NOT NULL,
    "custom_information_type" TEXT NOT NULL,
    "raw" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "custom_informations_data_pkey" PRIMARY KEY ("type_id","custom_information_type")
);

-- AddForeignKey
ALTER TABLE "custom_informations_data" ADD CONSTRAINT "custom_informations_data_type_id_fkey" FOREIGN KEY ("type_id") REFERENCES "DiaperType"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "custom_informations_data" ADD CONSTRAINT "custom_informations_data_custom_information_type_fkey" FOREIGN KEY ("custom_information_type") REFERENCES "customs_informations"("id") ON DELETE CASCADE ON UPDATE CASCADE;
