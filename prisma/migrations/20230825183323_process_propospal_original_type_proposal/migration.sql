-- AlterTable
ALTER TABLE "ProcessProposal" ADD COLUMN     "originalTypeProposalId" INTEGER;

-- AddForeignKey
ALTER TABLE "ProcessProposal" ADD CONSTRAINT "ProcessProposal_originalTypeProposalId_fkey" FOREIGN KEY ("originalTypeProposalId") REFERENCES "TypeProposal"("id") ON DELETE CASCADE ON UPDATE CASCADE;
