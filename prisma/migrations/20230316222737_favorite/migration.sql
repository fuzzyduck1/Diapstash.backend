-- CreateTable
CREATE TABLE "FavoriteType" (
    "user_id" TEXT NOT NULL,
    "type_id" INTEGER NOT NULL,

    CONSTRAINT "FavoriteType_pkey" PRIMARY KEY ("user_id","type_id")
);

-- AddForeignKey
ALTER TABLE "FavoriteType" ADD CONSTRAINT "FavoriteType_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "FavoriteType" ADD CONSTRAINT "FavoriteType_type_id_fkey" FOREIGN KEY ("type_id") REFERENCES "DiaperType"("id") ON DELETE CASCADE ON UPDATE CASCADE;
