-- CreateTable
CREATE TABLE "AdminAccount" (
    "username" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "requireChangePassword" BOOLEAN NOT NULL,

    CONSTRAINT "AdminAccount_pkey" PRIMARY KEY ("username")
);
