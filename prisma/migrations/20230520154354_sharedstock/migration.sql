-- CreateEnum
CREATE TYPE "SharedStockState" AS ENUM ('PENDING', 'ACCEPTED');

-- CreateTable
CREATE TABLE "SharedStock" (
    "id" SERIAL NOT NULL,
    "userId" TEXT NOT NULL,
    "securityToken" TEXT NOT NULL,
    "fromName" TEXT,
    "toName" TEXT NOT NULL,
    "state" "SharedStockState" NOT NULL,
    "readonly" BOOLEAN NOT NULL DEFAULT false,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3),

    CONSTRAINT "SharedStock_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "SharedStockAccess" (
    "userId" TEXT NOT NULL,
    "sharedStockId" INTEGER NOT NULL,

    CONSTRAINT "SharedStockAccess_pkey" PRIMARY KEY ("userId","sharedStockId")
);

-- AddForeignKey
ALTER TABLE "SharedStock" ADD CONSTRAINT "SharedStock_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SharedStockAccess" ADD CONSTRAINT "SharedStockAccess_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SharedStockAccess" ADD CONSTRAINT "SharedStockAccess_sharedStockId_fkey" FOREIGN KEY ("sharedStockId") REFERENCES "SharedStock"("id") ON DELETE CASCADE ON UPDATE CASCADE;
