/*
  Warnings:

  - You are about to drop the column `endTime` on the `DiaperChange` table. All the data in the column will be lost.
  - You are about to drop the column `startTime` on the `DiaperChange` table. All the data in the column will be lost.
  - You are about to drop the column `userId` on the `DiaperChange` table. All the data in the column will be lost.
  - You are about to drop the `_DiaperChangeToDiaperType` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `change_id` to the `DiaperChange` table without a default value. This is not possible if the table is not empty.
  - Added the required column `size` to the `DiaperChange` table without a default value. This is not possible if the table is not empty.
  - Added the required column `type_id` to the `DiaperChange` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "DiaperChange" DROP CONSTRAINT "DiaperChange_userId_fkey";

-- DropForeignKey
ALTER TABLE "_DiaperChangeToDiaperType" DROP CONSTRAINT "_DiaperChangeToDiaperType_A_fkey";

-- DropForeignKey
ALTER TABLE "_DiaperChangeToDiaperType" DROP CONSTRAINT "_DiaperChangeToDiaperType_B_fkey";

-- AlterTable
ALTER TABLE "DiaperChange" DROP COLUMN "endTime",
DROP COLUMN "startTime",
DROP COLUMN "userId",
ADD COLUMN     "change_id" TEXT NOT NULL,
ADD COLUMN     "size" TEXT NOT NULL,
ADD COLUMN     "type_id" INTEGER NOT NULL;

-- DropTable
DROP TABLE "_DiaperChangeToDiaperType";

-- CreateTable
CREATE TABLE "Change" (
    "id" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    "startTime" TIMESTAMP(3) NOT NULL,
    "endTime" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Change_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Change" ADD CONSTRAINT "Change_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "DiaperChange" ADD CONSTRAINT "DiaperChange_change_id_fkey" FOREIGN KEY ("change_id") REFERENCES "Change"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "DiaperChange" ADD CONSTRAINT "DiaperChange_type_id_fkey" FOREIGN KEY ("type_id") REFERENCES "DiaperType"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
