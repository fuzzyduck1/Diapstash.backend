-- DropForeignKey
ALTER TABLE "ProcessProposal" DROP CONSTRAINT "ProcessProposal_proposedTypeId_fkey";

-- AlterTable
ALTER TABLE "ProcessProposal" ADD COLUMN     "typeProposalId" INTEGER;

-- AddForeignKey
ALTER TABLE "ProcessProposal" ADD CONSTRAINT "ProcessProposal_proposedTypeId_fkey" FOREIGN KEY ("proposedTypeId") REFERENCES "DiaperType"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ProcessProposal" ADD CONSTRAINT "ProcessProposal_typeProposalId_fkey" FOREIGN KEY ("typeProposalId") REFERENCES "TypeProposal"("id") ON DELETE CASCADE ON UPDATE CASCADE;
