-- AlterTable
ALTER TABLE "TypeProposal" ADD COLUMN     "newTypeId" INTEGER;

-- AddForeignKey
ALTER TABLE "TypeProposal" ADD CONSTRAINT "TypeProposal_newTypeId_fkey" FOREIGN KEY ("newTypeId") REFERENCES "DiaperType"("id") ON DELETE SET NULL ON UPDATE CASCADE;
