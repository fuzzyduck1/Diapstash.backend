-- AlterTable
ALTER TABLE "DiaperType" ADD COLUMN     "hidden" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "replacedById" INTEGER;

-- AddForeignKey
ALTER TABLE "DiaperType" ADD CONSTRAINT "DiaperType_replacedById_fkey" FOREIGN KEY ("replacedById") REFERENCES "DiaperType"("id") ON DELETE SET NULL ON UPDATE CASCADE;
