-- CreateTable
CREATE TABLE "customs_informations" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "type" TEXT NOT NULL,
    "custom_data" JSONB NOT NULL,
    "user_id" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "customs_informations_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "custom_informations_type" (
    "type_id" INTEGER NOT NULL,
    "custom_information_type" TEXT NOT NULL,
    "raw" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "custom_informations_type_pkey" PRIMARY KEY ("type_id","custom_information_type")
);

-- AddForeignKey
ALTER TABLE "customs_informations" ADD CONSTRAINT "customs_informations_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "custom_informations_type" ADD CONSTRAINT "custom_informations_type_type_id_fkey" FOREIGN KEY ("type_id") REFERENCES "DiaperType"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "custom_informations_type" ADD CONSTRAINT "custom_informations_type_custom_information_type_fkey" FOREIGN KEY ("custom_information_type") REFERENCES "customs_informations"("id") ON DELETE CASCADE ON UPDATE CASCADE;
