-- CreateTable
CREATE TABLE "rating" (
    "id" TEXT NOT NULL,
    "criteria" TEXT NOT NULL,
    "note" DOUBLE PRECISION NOT NULL,
    "type_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "rating_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "rating_user" (
    "rating_id" TEXT NOT NULL,
    "user_id" TEXT NOT NULL,

    CONSTRAINT "rating_user_pkey" PRIMARY KEY ("rating_id","user_id")
);

-- AddForeignKey
ALTER TABLE "rating" ADD CONSTRAINT "rating_type_id_fkey" FOREIGN KEY ("type_id") REFERENCES "DiaperType"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "rating_user" ADD CONSTRAINT "rating_user_rating_id_fkey" FOREIGN KEY ("rating_id") REFERENCES "rating"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "rating_user" ADD CONSTRAINT "rating_user_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
