-- DropForeignKey
ALTER TABLE "Change" DROP CONSTRAINT "Change_user_id_fkey";

-- DropForeignKey
ALTER TABLE "DiaperChange" DROP CONSTRAINT "DiaperChange_type_id_fkey";

-- DropForeignKey
ALTER TABLE "DiaperStock" DROP CONSTRAINT "DiaperStock_diaperTypeId_fkey";

-- DropForeignKey
ALTER TABLE "DiaperType" DROP CONSTRAINT "DiaperType_userId_fkey";

-- AddForeignKey
ALTER TABLE "DiaperType" ADD CONSTRAINT "DiaperType_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "DiaperStock" ADD CONSTRAINT "DiaperStock_diaperTypeId_fkey" FOREIGN KEY ("diaperTypeId") REFERENCES "DiaperType"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Change" ADD CONSTRAINT "Change_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "DiaperChange" ADD CONSTRAINT "DiaperChange_type_id_fkey" FOREIGN KEY ("type_id") REFERENCES "DiaperType"("id") ON DELETE CASCADE ON UPDATE CASCADE;
