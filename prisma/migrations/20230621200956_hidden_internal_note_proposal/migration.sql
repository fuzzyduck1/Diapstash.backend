-- AlterTable
ALTER TABLE "Proposal" ADD COLUMN     "hidden" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "internalNote" TEXT;
