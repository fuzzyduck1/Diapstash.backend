-- CreateEnum
CREATE TYPE "ChangeState" AS ENUM ('DRY', 'WET', 'MESSY', 'WET_MESSY');

-- AlterTable
ALTER TABLE "Change" ADD COLUMN     "note" TEXT,
ADD COLUMN     "state" "ChangeState";
