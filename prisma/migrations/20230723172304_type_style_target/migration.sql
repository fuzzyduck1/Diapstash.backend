-- CreateEnum
CREATE TYPE "DiaperStyle" AS ENUM ('TABS', 'PULLUP');

-- CreateEnum
CREATE TYPE "DiaperTarget" AS ENUM ('ABDL', 'MEDICAL', 'YOUTH');

-- AlterTable
ALTER TABLE "DiaperType" ADD COLUMN     "style" "DiaperStyle",
ADD COLUMN     "target" "DiaperTarget"[];
