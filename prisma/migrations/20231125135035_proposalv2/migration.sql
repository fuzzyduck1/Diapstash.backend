-- CreateEnum
CREATE TYPE "ProposalOrigin" AS ENUM ('APP', 'CATALOG');

-- CreateTable
CREATE TABLE "proposal_v2" (
    "id" TEXT NOT NULL,
    "proposalOrigin" "ProposalOrigin" NOT NULL,
    "state" "ProposalState" NOT NULL,
    "app_user_id" TEXT,
    "admin_user_id" TEXT,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "proposal_v2_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "type_proposal_v2" (
    "id" TEXT NOT NULL,
    "proposal_id" TEXT NOT NULL,
    "original_type" INTEGER,
    "result_type" INTEGER,
    "name" TEXT NOT NULL,
    "brand_code" TEXT,
    "image" TEXT,
    "availableSizes" TEXT[] DEFAULT ARRAY[]::TEXT[],
    "type" "EType" NOT NULL DEFAULT 'DIAPER',
    "style" "DiaperStyle",
    "target" "DiaperTarget"[],
    "discontinued" BOOLEAN NOT NULL DEFAULT false,
    "comment" TEXT NOT NULL,
    "reason" TEXT,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "type_proposal_v2_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "type_process_proposal" (
    "id" TEXT NOT NULL,
    "type_proposal_id" TEXT NOT NULL,
    "original_type" INTEGER,
    "jsonPath" TEXT NOT NULL,
    "comment" TEXT NOT NULL,
    "state" "ProposalState" NOT NULL,
    "username" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "type_process_proposal_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "brand_proposal_v2" (
    "id" TEXT NOT NULL,
    "proposal_id" TEXT NOT NULL,
    "original_brand" TEXT,
    "result_brand" TEXT,
    "name" TEXT NOT NULL,
    "image" TEXT,
    "comment" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "brand_proposal_v2_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "brand_process_proposal" (
    "id" TEXT NOT NULL,
    "brand_proposal_id" TEXT NOT NULL,
    "original_brand" TEXT,
    "jsonPath" TEXT NOT NULL,
    "comment" TEXT NOT NULL,
    "state" "ProposalState" NOT NULL,
    "username" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "brand_process_proposal_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "type_proposal_v2_proposal_id_key" ON "type_proposal_v2"("proposal_id");

-- CreateIndex
CREATE UNIQUE INDEX "brand_proposal_v2_proposal_id_key" ON "brand_proposal_v2"("proposal_id");

-- AddForeignKey
ALTER TABLE "proposal_v2" ADD CONSTRAINT "proposal_v2_app_user_id_fkey" FOREIGN KEY ("app_user_id") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "proposal_v2" ADD CONSTRAINT "proposal_v2_admin_user_id_fkey" FOREIGN KEY ("admin_user_id") REFERENCES "AdminAccount"("username") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "type_proposal_v2" ADD CONSTRAINT "type_proposal_v2_proposal_id_fkey" FOREIGN KEY ("proposal_id") REFERENCES "proposal_v2"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "type_proposal_v2" ADD CONSTRAINT "type_proposal_v2_original_type_fkey" FOREIGN KEY ("original_type") REFERENCES "DiaperType"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "type_proposal_v2" ADD CONSTRAINT "type_proposal_v2_result_type_fkey" FOREIGN KEY ("result_type") REFERENCES "DiaperType"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "type_proposal_v2" ADD CONSTRAINT "type_proposal_v2_brand_code_fkey" FOREIGN KEY ("brand_code") REFERENCES "DiaperBrand"("code") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "type_process_proposal" ADD CONSTRAINT "type_process_proposal_type_proposal_id_fkey" FOREIGN KEY ("type_proposal_id") REFERENCES "type_proposal_v2"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "type_process_proposal" ADD CONSTRAINT "type_process_proposal_original_type_fkey" FOREIGN KEY ("original_type") REFERENCES "DiaperType"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "type_process_proposal" ADD CONSTRAINT "type_process_proposal_username_fkey" FOREIGN KEY ("username") REFERENCES "AdminAccount"("username") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "brand_proposal_v2" ADD CONSTRAINT "brand_proposal_v2_proposal_id_fkey" FOREIGN KEY ("proposal_id") REFERENCES "proposal_v2"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "brand_proposal_v2" ADD CONSTRAINT "brand_proposal_v2_original_brand_fkey" FOREIGN KEY ("original_brand") REFERENCES "DiaperBrand"("code") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "brand_proposal_v2" ADD CONSTRAINT "brand_proposal_v2_result_brand_fkey" FOREIGN KEY ("result_brand") REFERENCES "DiaperBrand"("code") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "brand_process_proposal" ADD CONSTRAINT "brand_process_proposal_brand_proposal_id_fkey" FOREIGN KEY ("brand_proposal_id") REFERENCES "brand_proposal_v2"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "brand_process_proposal" ADD CONSTRAINT "brand_process_proposal_original_brand_fkey" FOREIGN KEY ("original_brand") REFERENCES "DiaperBrand"("code") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "brand_process_proposal" ADD CONSTRAINT "brand_process_proposal_username_fkey" FOREIGN KEY ("username") REFERENCES "AdminAccount"("username") ON DELETE CASCADE ON UPDATE CASCADE;
