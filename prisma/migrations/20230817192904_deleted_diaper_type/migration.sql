-- CreateTable
CREATE TABLE "deleted_diaper_type" (
    "id" SERIAL NOT NULL,
    "diaper_type_id" INTEGER NOT NULL,
    "user_id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "deleted_diaper_type_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "deleted_diaper_type" ADD CONSTRAINT "deleted_diaper_type_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
