-- CreateTable
CREATE TABLE "whats_news_messages" (
    "id" SERIAL NOT NULL,
    "whats_news_id" INTEGER NOT NULL,
    "lang" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT NOT NULL,

    CONSTRAINT "whats_news_messages_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "whats_news_messages_whats_news_id_lang_key" ON "whats_news_messages"("whats_news_id", "lang");

-- AddForeignKey
ALTER TABLE "whats_news_messages" ADD CONSTRAINT "whats_news_messages_whats_news_id_fkey" FOREIGN KEY ("whats_news_id") REFERENCES "whats_news"("id") ON DELETE CASCADE ON UPDATE CASCADE;
