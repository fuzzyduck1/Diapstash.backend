import { parseArgs } from 'node:util'
import * as path from "path";
import * as fs from "fs/promises";
import { PrismaClient, Prisma } from '@prisma/client'

const prisma = new PrismaClient()

async function main() {
    const {
        values: { lite },
    } = parseArgs({
        options: {
            lite: { type: 'boolean', default: false, short: 'l' },
        },
    });

    let seedFolder = "./data/seed";

    if (lite) {
        seedFolder = path.join(seedFolder, 'lite_types');
    } else {
        seedFolder = path.join(seedFolder, 'full_types');

    }

    let files = await fs.readdir(seedFolder, { recursive: false });
    files = files.filter(f => f.endsWith(".sql")).sort((a, b) => a.localeCompare(b));
    for (let file of files) {
        let fullPath = path.join(seedFolder, file);
        console.log(`Load ${fullPath}`);
        let raws = (await fs.readFile(fullPath)).toString()
            .split(';\n');
        for (let raw of raws) {
            await prisma.$queryRawUnsafe(raw);

        }

    }
}

main()
    .then(async () => {
        await prisma.$disconnect()
    })
    .catch(async (e) => {
        console.error(e)
        await prisma.$disconnect()
        process.exit(1)
    })
