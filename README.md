# DiapStash - Backend
DiapStash © 2023 by RanTigr is licensed under CC BY-NC-ND 4.0

> **Distribution of the application outside the official channels (Android Play Store & https://app.diapstash.com) is strictly forbidden, including for a modified version of the application!**

[Website](https://diapstash.com) - [Android App](https://play.google.com/store/apps/details?id=fr.diapstash.diap_stash) - [Web App (_unstable_)](https://app.diapstash.com)

[Reddit community](https://reddit.com/r/diapstash/) - [Help to translate on Crowdin](https://crowdin.com/project/diapstash) - [Gitlab](https://gitlab.com/diapstash)

Track your stash
Diaper Stock Management for ABDL and incontient

The application allows the tracking of the stock of diapers, the tracking of the history of diaper changes.

Created by [RanTigr](https://linktr.ee/rantigr).

## Informations

This repository concerns the server side of the DiapStash application. The application source can be found on [gitlab.com/diapstash/DiapStash](https://gitlab.com/diapstash/DiapStash).

The DiapStash backend manages the catalog (brands, types, proposals) and the "cloud-sync" part, enabling users to save their data remotely and share it across multiple devices.


## Development / Contributing

### Requirements

* nodejs : lts
* yarn
* postgres database (docker recommanded)
* Minio / S3 bucket (optional, only for image upload)

### Guidelines

* Each database schema modification must be done via migration [prisma migrate](https://www.prisma.io/docs/concepts/components/prisma-migrate).

### Dev

#### Before run

1. Create `.env` from the exemple `.env.exemple`
2. Run postgres server (`docker compose up -d`)
3. Edit `.env` with your env settings
4. Generate prisma files : `yarn prisma generate`
5. [Seed your databse](#seed-database)


### Commands

### Start server in dev

```
yarn start
```

### Generate prisma file
```
yarn prisma generate
```

### Generate prisma migration
```
yarn prisma migrate dev --name "<what do your migration>"
```

### Seed database

```
yarn prisma:seed
```

If you want a small number of types

```
yarn prisma:seed:lite
```

### Git

#### Branches

* `main` : The `main` branch are the "production" branch.
* Development are do on feature branch

#### Naming convention for commits

DiapStash repositories use [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/). Please respect it !

### Configurations

Definition of .env values

#### `DATABASE_URL` & `DIRECT_DATABASE_URL`

Postgres database url

#### `S3_HOST`, `S3_BUCKET`, `S3_ACCESS_KEY` & `S3_SECRET_KEY`

Credentials for storage image buckets.

DiapStash use [MinIO](https://min.io/) in productions

#### `IMAGE_URL`

Public URL of storage image bucket

#### `NO_CACHE_DOMAIN`

Domain for bypass cloudflare cache

#### `REMOVE_BG_API`

[remove.bg](https://remove.bg) api key for remove background on type's images.

#### `CROWDIN_TOKEN` &  `CROWDIN_PROJECT_ID`

Crowdin api crendentials for gettings What's New translations.